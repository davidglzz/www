document.querySelectorAll('.menu').forEach(btn => {
    btn.addEventListener('click', e => {
        btn.classList.toggle('active');
    });
});

var step=0, number2=-1, precioinicial=598000, preciomotor, preciocolor, precioteca, precioaddons=0, preciofinal, fuel, numcolor=0, color, numtapi=1, numtecho = 1; tapi="Diamante Ivory", teca="TEAK", addons = [];

$(window).on("load",function(){
    $(".loader-wrapper").delay(2000).animate({width: '0px', left: '-20px'}, 800);
});

//Funcion cambio de color y tapiceria:

function changeColorBack(a, b, c) {
    return "/assets/img/say42_back/"+a+"/"+c+"/"+b+".png"
};

function changeColorFront(a, b, c) {
    return "/assets/img/say42_front/"+a+"/"+c+"/"+b+".png"
};

function changeColorTop(a, b, c) {
    return "/assets/img/say42_top/"+a+"/"+c+"/"+b+".png"
};

function breathSubtitle(text){
  $(".subtitle").animate({fontSize: '+=5rem!important'}, 500);
  $(".subtitle").text(text);
  $(".subtitle").animate({fontSize: '-=5rem!important'}, 500);
}

function breath(){
  $(".content").fadeOut();
  $(".content").fadeIn();
  $(".summary").fadeOut();
  $(".summary").fadeIn();
}

function upDownColor(number){
  if(number!=number2){
    $("#option-color1").slideUp(1).animate({top: '500'},1);
    $("#option-color2").slideUp(1).animate({top: '500'},1);
    $("#option-color3").slideUp(1).animate({top: '500'},1);
    $("#option-color4").slideUp(1).animate({top: '500'},1);
    $("#option-color5").slideUp(1).animate({top: '500'},1);
    $("#option-color6").slideUp(1).animate({top: '500'},1);
    $("#option-color7").slideUp(1).animate({top: '500'},1);
    $("#option-color8").slideUp(1).animate({top: '500'},1);
    $("#option-color9").slideUp(1).animate({top: '500'},1);
    $("#option-color10").slideUp(1).animate({top: '500'},1);
    $("#option-color11").slideUp(1).animate({top: '500'},1);
    $("#option-color12").slideUp(1).animate({top: '500'},1);
    $("#option-color13").slideUp(1).animate({top: '500'},1);
  }

  if(number2==-1){
    $("#option-color1").slideDown(1).animate({top: '0'},1000);
    $("#option-color2").slideDown(1).animate({top: '0'},1200);
    $("#option-color3").slideDown(1).animate({top: '0'},1400);
    $("#option-color4").slideDown(1).animate({top: '0'},1600);
      number2=0;
  }else if(number==0 && number!=number2){
    $("#option-color1").slideDown(1).animate({top: '0'},400);
    $("#option-color2").slideDown(1).animate({top: '0'},600);
    $("#option-color3").slideDown(1).animate({top: '0'},800);
    $("#option-color4").slideDown(1).animate({top: '0'},1000);
    number2 = number;
  }else if(number==1 && number!=number2){
    $("#option-color5").slideDown(1).animate({top: '0'},400);
    $("#option-color6").slideDown(1).animate({top: '0'},600);
    $("#option-color7").slideDown(1).animate({top: '0'},800);
    number2 = number;
  }else if(number==2 && number!=number2){
    $("#option-color8").slideDown(1).animate({top: '0'},400);
    $("#option-color9").slideDown(1).animate({top: '0'},600);
    $("#option-color10").slideDown(1).animate({top: '0'},800);
    number2 = number;
  }else if (number==3 && number!=number2){
    $("#option-color11").slideDown(1).animate({top: '0'},400);
    $("#option-color12").slideDown(1).animate({top: '0'},600);
    $("#option-color13").slideDown(1).animate({top: '0'},800);
    number2 = number;
  }
}

function extrasSelected(){
  addons = [];
  $('.addons').text("");
  $(".btn-success").each(function(){
    var id = $(this).attr('id');
    var text = $("#"+id).text();
    addons.push(text);
    $('.addons').append("<br> Extra: "+text);
  });

  $('.summary').animate({height: "100vh"});
}

function extraSelected(){
  $('.addons').text("");
  $(".btn-success").each(function(){
    var id = $(this).attr('id');
    var text = $("#"+id).text();
    addons.push(text);
    $('#subtitle').append("<br> Extra: "+text);
  });
}

function addValues(numero){
  var addon = "";
  for (var i=0; i<addons.length; i++) { 
    addon += addons[i]+' <br>';
  }

  $('#yachtform').val('SAY 42R');
  $('#engineform').val(fuel);
  $('#colourform').val(color);
  $('#upholsteryform').val(tapi);
  $('#floorform').val(teca);
  $('#addonsform').val(addon);
  $('#priceform').val(numero);
  $('#numcolor').val(numcolor);
  $('#numtapi').val(numtapi);
  $('#numtecho').val(numtecho);
}

$(document).ready(

	function(){
    $(".price").text(precioinicial.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    $('#botonMenu').slideUp();
    $('.masthead').animate({"opacity": 1},1000);
    //$('.carousel').slideUp("fast");
    $('.option-group-color').slideUp("fast");
    $('.option-color').slideUp("fast");
    $('.option-tapiceria').slideUp("fast");
    $('.option-teca').slideUp("fast");
    $('.formulario').slideUp("fast");
    $('.option-addons').slideUp("fast");
    $('#add').slideUp("fast");
    $('#comfort').slideUp("fast");
    $('#hard').slideUp("fast");
    $('#acce').slideUp("fast");
    $('#roof').slideUp("fast");

    $('.btn-next').attr("disabled", true);


    $(".btn-cerrar").click(function(){
      $('.lateral').animate({"right": -900});
      $('.main').animate({left: '0vw'});
      $('.main').animate({width: '100vw'});
      $('.customize').animate({left: '7vw'});
      $('.customize').animate({width: '100vw'});
      $('.options').addClass('invisible');
      $('.option-overlay').animate({left: '-200px'});
      $('.titulo').addClass('invisible');
      if(step==2){
        $('.option-teca').slideUp("fast");
      }
    });

    $(".btn-more").click(function(){
      $('.lateral').animate({"right": 0});
      $('.main').animate({left: '-4vw'});
      $('.main').animate({width: '70vw'});
      $('.customize').animate({left: '-4vw'});
      $('.customize').animate({width: '70vw'});
      $('.options').removeClass('invisible');
      $('.option-overlay').animate({left: '-400px'});
      $('.titulo').removeClass('invisible');
      if(step==2){
        $('.option-teca').slideDown("fast");
      }
    });

    $("#down").click(function(){
        var y = $(".option-text").scrollTop();
        $('.option-text').animate({scrollTop: y + 500 });
        console.log(y);
     });

    $("#up").click(function(){
        var y = $(".option-text").scrollTop();
        $('.option-text').animate({scrollTop: y - 500 });
        console.log(y);
     });

    $("#roof1").click(function(){
        var y = $('HTML, BODY').scrollTop();
        $('HTML, BODY').animate({scrollTop: y + 1000 });
        console.log(y);
     });

    $("#roof2").click(function(){
        var y = $('HTML, BODY').scrollTop();
        $('HTML, BODY').animate({scrollTop: y + 1000 });
        console.log(y);
     });

    $("#roof3").click(function(){
        var y = $('HTML, BODY').scrollTop();
        $('HTML, BODY').animate({scrollTop: y + 1000 });
        console.log(y);
     });

    $("#roof4").click(function(){
        var y = $('HTML, BODY').scrollTop();
        $('HTML, BODY').animate({scrollTop: y + 1000 });
        console.log(y);
     });



    //Menú derecho
    $('.btn-next').click(function(){
      switch(step){
        //COLOR
        case 0: $('.btn-back').removeClass("disabled");
                $('.btn-next').attr("disabled", true);
                $('.option-fuel').slideUp(500);
                $('.option-group-color').delay(500).slideDown(500);
                $('.option-color').delay(500).slideDown(1);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 42R Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.");
                breathSubtitle("SAY 42R: COLOURSCHEME");
                breath();
                upDownColor(0);
                break;
        //UPHOLSTERY
        case 1: $('.btn-back').removeClass("disabled");
                $('.btn-next').attr("disabled", true);
                $('.option-group-color').slideUp(1000)
                $('.option-color').slideUp(1000);
                $('.option-tapiceria').delay(1000).slideDown(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.color').text("Colourscheme: "+color);
                $("#title").text("SAY 42R Carbon");
                $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.");
                breathSubtitle("SAY 42R: FLOOR AND UPHOLSTERY");
                breath();
                break;
        //EXTRAS
        case 2: $('.btn-next').attr("disabled", false);
                $('#myCarousel').fadeOut().slideUp();
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.option-addons').delay(1000).slideDown(1000);
                $('#add').slideDown(500);
                $(".opt-2").slideUp("fast");
                $(".opt-3").slideUp("fast");
                $(".opt-4").slideUp("fast");
                $(".opt-5").slideUp("fast");
                $('.tapi').text("Upholstery: "+tapi);
                $('.teca').text("Floor: "+teca);
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                breathSubtitle("SAY 42R: ADD-ONS");
                breath();
                break;
        //SUMMARY
        case 3: $('.btn-next').attr("disabled", false);
                $('.option-addons').slideUp();
                $('.content').slideUp("fast");
                $('#myCarousel').delay(400).slideDown();
                $('.formulario').delay(400).slideDown();
                $('.btn-next').text("SEND NOW");
                $('.btn-next').addClass('invisible');
                breathSubtitle("SAY 42R: SUMMARY");
                extrasSelected();
                break;
      }
      step++;
    });

    $('.btn-back').click(function(){
      step--;
      switch(step){
        //ENGINE
        case 0: $('.btn-back').attr("disabled", true);
                $('.btn-next').attr("disabled", true);
                $('.option-color').slideUp(500);
                $('.option-group-color').slideUp(500);
                $('.option-fuel').delay(500).slideDown(500);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 42R Carbon");
                $("#subtitle").text("The new SAY 42R Carbon is another breakthrough of low-emission cruising with high-performance handling. Its lightweight hull design enabling speeds above 50 knots (Volvo Penta).");
                breathSubtitle("SAY 42R: ENGINE");
                breath();
                upDownColor(0);
                break;
        //COLOR
        case 1: $('.btn-next').attr("disabled", true);
                $('.option-group-color').delay(1000).slideDown(1000)
                $('.option-color').delay(1000).slideDown(1000);
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.color').text("Colourscheme: "+color);
                $("#title").text("SAY 42R Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.");
                breathSubtitle("SAY42: COLOURSCHEME");
                breath();
                break;
        //UPHOLSTERY
        case 2: $('.btn-next').attr("disabled", false);
                $('#myCarousel').delay(1000).fadeIn().slideDown();
                $('.option-addons').slideUp(1000);
                $('.option-tapiceria').delay(1000).slideDown(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.tapi').text("Upholstery: "+tapi);
                $('.teca').text("Floor: "+teca);
                $("#title").text(tapi+" + "+teca);
                $("#subtitle").text("");
                breath();
                break;
        //EXTRAS
        case 3: $('.btn-next').attr("disabled", false);
                $('.option-addons').delay(400).slideDown();
                $('.content').slideDown(400);
                $('#myCarousel').slideUp();
                $('.formulario').slideUp();
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                $('.btn-next').text("NEXT");
                $('.btn-next').removeClass('invisible');
                $('.summary').css({height: ""});
                breathSubtitle("SAY42: ADD-ONS");
                break;
      }
    });

    // OPCIONES FUEL
		$("#option-fuel1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      fuel = "2 x Mercruiser V8 6.2l 350hp ";
      preciomotor = 83200.00;
      $("#title").text(fuel);
      $("#subtitle").text('Mpi Sea Corencl. Bravo3 drive.').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
		});

    $("#option-fuel2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      fuel = "2 x Volvo Penta V8-430-CE DPS 6.2l 430hp";
      $("#title").text(fuel);
      preciomotor = 95812.00;
      $("#subtitle").text('Including Aquamatic duo-prop drive.').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

    $("#option-fuel3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      fuel = "2 x Ilmor MV8 7.4l";
      $("#title").text(fuel);
      preciomotor = 101850.00;
      $("#subtitle").text('483hp, incl. Ilmor One Drive.').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

    $("#option-fuel4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      fuel = "2 x Yanmar 4.5l 8LV-370 Diesel, 370hp.";
      $("#title").text(fuel);
      preciomotor = 113400.00;
      $("#subtitle").text('Incl. Yanmar ZT370 sterndrive.').append('<br><br>+'+preciomotor.toLocaleString()+' € <i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });





    //OPCIONES GRUPO COLOR

    $("#option-group-color1").click(function(){
      $("#title").text("METALLIC COLLECTION");
      $("#subtitle").text("Enjoy mesmerizing recflections of turqouise waters in Ibiza thanks to your glossy metallic painted hull.");
      upDownColor(0);
    });

    $("#option-group-color2").click(function(){
      $("#title").text("SAY LIFESTYLE");
      $("#subtitle").text("");
      upDownColor(1);
    });

    $("#option-group-color3").click(function(){
      $("#title").text("PURE COLLECTION");
      $("#subtitle").text("For puristic design lovers.");
      upDownColor(2);
    });

    $("#option-group-color4").click(function(){
      $("#title").text("RACING COLLECTION");
      $("#subtitle").text("Who says racing colours only exist on our roads?");
      upDownColor(3);
    });





    //OPCIONES COLOR
    $("#option-color1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Cannes (Champagne)";
      $("#title").text('Cannes (Champagne)');
      preciocolor = 8360.50;
      $("#subtitle").text('Shiny like a golden sunset in Cannes. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 1;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Milan (Platinum)";
      $("#title").text('Milan (Platinum)');
      preciocolor = 8360.50;
      $("#subtitle").text('Timeless and luxurious. Always an exquisite choice. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 2;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Ibiza (Blue)";
      $("#title").text('Ibiza (Blue)');
      preciocolor = 8360.50;
      $("#subtitle").text('Perfect interaction with the crytal clear waters in Ibiza and Formentera. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 3;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Ocean (Royal Blue)";
      $("#title").text('OCEAN (Royal Blue)');
      preciocolor = 8360.50;
      $("#subtitle").text('Pure and timesless elegance. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 4;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color5").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Carrara Blue";
      $("#title").text('Carrara Blue');
      preciocolor = 0;
      $("#subtitle").text('A delicate tone of blue to mark the difference.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 5;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color6").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Ambitious Green";
      $("#title").text('Ambitious Green');
            preciocolor = 0;
      $("#subtitle").text('Perfect match to your distinctive life-style.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 6;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color7").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Sky Blue";
      $("#title").text('Sky Blue');
      preciocolor = 0;
      $("#subtitle").text('This happy feeling when you look into the blue endless sky... ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 7;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color8").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "White";
      $("#title").text('White');
      preciocolor = 0;
      $("#subtitle").text('100% Purity: The positive and perfect classic.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 8;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color9").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Crayon Grey";
      $("#title").text('Crayon Grey');
      preciocolor = 0;
      $("#subtitle").text('Our favourite all-rounder. Pure Elegance.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 9;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color10").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Anthracite";
      $("#title").text('Anthracite');
      preciocolor = 0;
      $("#subtitle").text('Modern. Sophisticated. Timeless!').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 10;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color11").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Dynamic Orange";
      $("#title").text('Dynamic Orange');
      preciocolor = 0;
      $("#subtitle").text('Full of energy. Full of motivation. Let’s go yachting!').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 11;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color12").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Racing Green";
      $("#title").text('Racing Green');
      preciocolor = 0;
      $("#subtitle").text('This colour on your speed cruiser promises what it stands for.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 12;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-color13").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      color = "Miami Blue";
      $("#title").text('Miami Blue');
      preciocolor = 0;
      $("#subtitle").text('The colour to cheer you up! Who fancy being the eye catcher? ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 13;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });





    //OPCIONES TAPICERIA
    $("#option-tapiceria1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Diamante Ivory";
      $("#title").text(tapi+" + "+teca);
      numtapi = 1;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Hitch Cerulean";
      $("#title").text(tapi+" + "+teca);
      numtapi = 2;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Diamante Sage";
      $("#title").text(tapi+" + "+teca);
      numtapi = 3;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Silvertex Plata";
      $("#title").text(tapi+" + "+teca);
      numtapi = 4;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria5").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Silvertex Ice";
      $("#title").text(tapi+" + "+teca);
      numtapi = 5;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria6").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Silvertex Taupe";
      $("#title").text(tapi+" + "+teca);
      numtapi = 6;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria7").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Diamante Tonic";
      $("#title").text(tapi+" + "+teca);
      numtapi = 7;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });

    $("#option-tapiceria8").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "Diamante Carbon";
      $("#title").text(tapi+" + "+teca);
      numtapi = 8;
      $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
    });





    //OPCIONES TECA
    $("#option-teca1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      teca = "TEAK";
      $("#title").text(tapi+" + "+teca); 
      precioteca = 1217.64;
      $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.").append('<br><br>+'+precioteca.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

     $("#option-teca2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      teca = "SEATEK";
      $("#title").text(tapi+" + "+teca);
      precioteca = 3604.20;
      $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.").append('<br><br>+'+precioteca.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
     });


     //OPCION OVERLAY
     $(".opt-1").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#2").css("background", "#494949");
        $("#add").css("background", "#5B5B5B");
        $("#hard").slideDown(500);
        $("#comfort").slideUp(500);
        $("#acce").slideUp(500);
        $(".opt-1").slideUp(1);
        $(".opt-2").slideDown(500);
        $(".opt-3").slideDown(500);
        $(".opt-4").slideDown(500);
        $(".opt-5").slideDown(500);
        $("myCarousel").slideUp("fast");
     });

     $(".opt-2").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideDown(500);
        $("#roof").slideUp(500);
        $("#2").css("background", "#494949");
        $("#hard").css("background", "#5B5B5B");
        $("#comfort").slideUp(500);
        $("#acce").slideUp(500);
        $("myCarousel").slideUp("fast");
     });

     $(".opt-3").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideUp(500);
        $("#comfort").slideDown(500);
        $("#roof").slideUp(500);
        $("#3").css("background", "#E8E8E8");
        $("#comfort").css("background", "#5B5B5B");
        $("#acce").slideUp(500);
        $("myCarousel").slideUp("fast");
     });

     $(".opt-4").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideUp(500);
        $("#comfort").slideUp(500);
        $("#acce").slideDown(500);
        $("#roof").slideUp(500);
        $("#4").css("background", "#494949");
        $("#acce").css("background", "#5B5B5B");
        $("myCarousel").slideUp("fast");
     });

     $(".opt-5").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideUp(500);
        $("#comfort").slideUp(500);
        $("#acce").slideUp(500);
        $("#roof").slideDown(500);
        $("#4").css("background", "#E8E8E8");
        $("#acce").css("background", "#5B5B5B");
        $("myCarousel").fadeIn().slideDown("fast");
     });

     $("#hard1").click(function(){
      $("#hard1").toggleClass("btn-light").toggleClass("btn-success");
        if ($("#hard1").hasClass("btn-success")) {
          precioaddons += 3604.20;
        }else{
          precioaddons -= 3604.20;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(3604.20.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#hard2").click(function(){
      $("#hard2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#hard2").hasClass("btn-success")) {
          precioaddons += 6970.59;
        }else{
          precioaddons -= 6970.59;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(6970.59.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort1").click(function(){
      $("#comfort1").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort1").hasClass("btn-success")) {
          precioaddons += 3860.00;
        }else{
          precioaddons -= 3860.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(3860.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort2").click(function(){
      $("#comfort2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort2").hasClass("btn-success")) {
          precioaddons += 6700.00;
        }else{
          precioaddons -= 6700.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(6700.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort3").click(function(){
      $("#comfort3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort3").hasClass("btn-success")) {
          precioaddons += 5260.00;
        }else{
          precioaddons -= 5260.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(5260.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort4").click(function(){
      $("#comfort4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort4").hasClass("btn-success")) {
          precioaddons += 2850.60;
        }else{
          precioaddons -= 2850.60;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(2850.60.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort5").click(function(){
      $("#comfort5").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort5").hasClass("btn-success")) {
          precioaddons += 9827.73;
        }else{
          precioaddons -= 9827.73;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(9827.73.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort6").click(function(){
      $("#comfort6").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort6").hasClass("btn-success")) {
          precioaddons += 167.23;
        }else{
          precioaddons -= 167.23;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(167.23.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort7").click(function(){
      $("#comfort7").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort7").hasClass("btn-success")) {
          precioaddons += 747.90;
        }else{
          precioaddons -= 747.90;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(747.90.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort8").click(function(){
      $("#comfort8").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort8").hasClass("btn-success")) {
          precioaddons += 6250.00;
        }else{
          precioaddons -= 6250.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(6250.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort9").click(function(){
      $("#comfort9").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort9").hasClass("btn-success")) {
          precioaddons += 1595.80;
        }else{
          precioaddons -= 1595.80;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(1595.80.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort10").click(function(){
      $("#comfort10").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort10").hasClass("btn-success")) {
          precioaddons += 3503.36;
        }else{
          precioaddons -= 3503.36;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(3503.36.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort11").click(function(){
      $("#comfort11").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort11").hasClass("btn-success")) {
          precioaddons += 3234.45;
        }else{
          precioaddons -= 3234.45;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(411.76.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort12").click(function(){
      $("#comfort12").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort12").hasClass("btn-success")) {
          precioaddons += 43860.00;
        }else{
          precioaddons -= 43860.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(1436.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort13").click(function(){
      $("#comfort13").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort13").hasClass("btn-success")) {
          precioaddons += 32500.00;
        }else{
          precioaddons -= 32500.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(32500.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });
     $("#comfort14").click(function(){
      $("#comfort14").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort14").hasClass("btn-success")) {
          precioaddons += 4980.00;
        }else{
          precioaddons -= 4980.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(4980.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });
     $("#comfort15").click(function(){
      $("#comfort15").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort15").hasClass("btn-success")) {
          precioaddons += 2460.00;
        }else{
          precioaddons -= 2460.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(2460.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

    $("#comfort16").click(function(){
      $("#comfort16").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort16").hasClass("btn-success")) {
          precioaddons += 2632.00;
        }else{
          precioaddons -= 2632.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(2632.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce1").click(function(){
      $("#acce1").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce1").hasClass("btn-success")) {
          precioaddons += 898.32;
        }else{
          precioaddons -= 898.32;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(898.32.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce2").click(function(){
      $("#acce2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce2").hasClass("btn-success")) {
          precioaddons += 3777.31;
        }else{
          precioaddons -= 3777.31;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(3777.31.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce3").click(function(){
      $("#acce3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce3").hasClass("btn-success")) {
          precioaddons += 3016.80;
        }else{
          precioaddons -= 3016.80;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(3016.80.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce4").click(function(){
      $("#acce4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce4").hasClass("btn-success")) {
          precioaddons += 8394.96;
        }else{
          precioaddons -= 8394.96;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(8394.96.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce5").click(function(){
      $("#acce5").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce5").hasClass("btn-success")) {
          precioaddons += 839.50;
        }else{
          precioaddons -= 839.50;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(839.50.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce6").click(function(){
      $("#acce6").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce6").hasClass("btn-success")) {
          precioaddons += 460.20;
        }else{
          precioaddons -= 460.20;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(460.20.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#roof1").click(function(){
      $("#roof1").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#roof1").hasClass("btn-success")) {
          numtecho = 2;
          precioaddons += 28192.44;
          $("#roof2").slideUp();
          $("#roof3").slideUp();
          $("#roof4").slideUp();
        }else{
          numtecho = 1;
          precioaddons -= 28192.44;
          $("#roof2").slideDown();
          $("#roof3").slideDown();
          $("#roof4").slideDown();
        } 

        console.log(numtecho);

        $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
        $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
        $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(28192.44.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#roof2").click(function(){
      $("#roof2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#roof2").hasClass("btn-success")) {
        numtecho = 2;
          precioaddons += 33260.00;
          $("#roof1").slideUp();
          $("#roof3").slideUp();
          $("#roof4").slideUp();
        }else{
          numtecho = 1;
          precioaddons -= 33260.00;
          $("#roof1").slideDown();
          $("#roof3").slideDown();
          $("#roof4").slideDown();
        }

        $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
        $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
        $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(33260.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#roof3").click(function(){
      $("#roof3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#roof3").hasClass("btn-success")) {
          numtecho = 3;
          precioaddons += 9860.00;
          $("#roof1").slideUp();
          $("#roof2").slideUp();
          $("#roof4").slideUp();
        }else{
          numtecho = 1;
          precioaddons -= 9860.00;
          $("#roof1").slideDown();
          $("#roof2").slideDown();
          $("#roof4").slideDown();
        }

        $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
        $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
        $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(9860.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#roof4").click(function(){
      $("#roof4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#roof4").hasClass("btn-success")) {
          numtecho = 3;
          precioaddons += 18440.00;
          $("#roof1").slideUp();
          $("#roof2").slideUp();
          $("#roof3").slideUp();
        }else{
          numtecho = 1;
          precioaddons -= 18440.00;
          $("#roof1").slideDown();
          $("#roof2").slideDown();
          $("#roof3").slideDown();
        }

        $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
        $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
        $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        
        $("#subtitle").append(18440.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });


     $('.btn-order').click(function(){
      preciofinal = (precioinicial+preciomotor+preciocolor+precioteca+precioaddons);
      addValues(preciofinal);
     });
  }   
);