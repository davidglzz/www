<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() 
        { 
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('form');
        } 

	public function index()
	{
		$header['title'] = 'Say Yachts';
		$header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/main.css';
		$body['js'] = base_url().'assets/js/main.js';
		$body['video_main'] = base_url().'assets/img/video_main.mp4';
		$body['video_full'] = base_url().'assets/img/video_full.mp4';
		$this->load->view('index/header', $header);
		$this->load->view('main', $body);		
	}

	public function customize()
	{
		$header['title'] = 'Say Yachts';
		$header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/customize.css';
		$body['js'] = base_url().'assets/js/customize.js';
		$body['yacht29'] = base_url().'assets/img/say29.png';
		$body['yacht42'] = base_url().'assets/img/say42.png';
		$body['yacht45'] = base_url().'assets/img/say45.png';
		$this->load->view('index/header', $header);
		$this->load->view('customize', $body);	
	}

	public function say29()
	{
		$header['title'] = 'SAY 29 CARBON';
		$header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/customize29.css';
		$body['js'] = base_url().'assets/js/customize29.js';
		$this->load->view('index/header', $header);
		$this->load->view('customize29', $body);	
	}

	public function say42()
	{
		$header['title'] = 'SAY 42R CARBON';
		$header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/customize42.css';
		$body['js'] = base_url().'assets/js/customize42.js';
		$this->load->view('index/header', $header);
		$this->load->view('customize42', $body);	
	}

	public function say45()
	{
		$header['title'] = 'SAY 45 RIB';
		$header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/customize45.css';
		$body['js'] = base_url().'assets/js/customize45.js';
		$this->load->view('index/header', $header);
		$this->load->view('customize45', $body);	
	}

  public function news(){
    $email = $this->input->post("email");
    $data = array(
        'email' => $email
      );  
    $this->db->insert('newsletter', $data);

    $this->load->view('order');

  }

	public function order()
		{	
		$titulo = $this->input->post("title");
		$nombre = $this->input->post("first");
		$apellido = $this->input->post("last");
		$ciudad = $this->input->post("city");
		$pais = $this->input->post("country");
		$correo = $this->input->post("email");
		$telefono = $this->input->post("phone");
		$yacht = $this->input->post("yacht");
		$price = $this->input->post("precio");
		$news = $this->input->post("news");
		$numcolor = $this->input->post("numcolor");
		$numtapi = $this->input->post("numtapi");
		$price = (float)$price;
		$price = number_format($price, 2, ',', '.');

		if($yacht=="SAY 29"){
			$url = "say29";
		}else if($yacht=="SAY 42R"){
			$roof = $this->input->post("numtecho");
			$url = "say42";
		}else{
			$url = "say45";
		}

		if($url == 'say29'){
			$data1 = strtolower($url)."_back/$numtapi/$numcolor.png";
			$data2 = strtolower($url)."_front/$numtapi/$numcolor.png";
			$data3 = strtolower($url)."_top/$numtapi/$numcolor.png";
		}else{
			$data1 = strtolower($url)."_back/$roof/$numtapi/$numcolor.png";
			$data2 = strtolower($url)."_front/$roof/$numtapi/$numcolor.png";
			$data3 = strtolower($url)."_top/$roof/$numtapi/$numcolor.png";
		}

		$customer = array(
			'title' => $titulo,
			'firstname' => $nombre,
			'lastname' => $apellido,
			'city' => $ciudad,
			'country' => $pais,
			'email' => $correo,
			'phone' => $telefono
		);

		if(isset($news) && $news == "on"){
			$data = array(
				'email' => $correo
			);	
			$this->db->insert('newsletter', $data);
		}

		if(isset($yacht) && $yacht!='SAY 45 RIB'){
			$engine = $this->input->post("engine");
			$colour = $this->input->post("colourscheme");
			$upholstery = $this->input->post("upholstery");
			$floor = $this->input->post("floor");
			$addon = $this->input->post("addon");

				$this->load->library('pdf');
				$dompdf = new PDF();
				$dompdf->setPaper('A4', 'portrait');
				$dompdf->set_option('isRemoteEnabled', TRUE);
				ob_start();
				$html = "<html>
					<head>
						<meta charset=\"UTF-8\"/>
						<title>SAY Yachts</title>
						<style type=\"text/css\">
							@font-face {
				    				font-family: Watchword;
				    				src: url(\"/assets/fonts/watchword.otf\");
							}

							@font-face {
				    				font-family: GillSans;
				    				src: url(\"/assets/fonts/watchword.otf\");
							}

							h2{
								color: blue;
							}

							.logo{
								text-align: right;
								margin: 1% 3%;
							}

							.logo img{
								width: 35%;
								height: 15%;
							}

							.subtitle{
								margin-top: 60px;
								text-align: center;
								font-size: 20px;
								letter-spacing: 2px;
								font-family: Watchword;
								font-weight: lighter;
							}

							.title{
								text-align: center;
								margin: 30px;
								font-size: 30px;
								font-family: GillSans;
							}

							.summary{
								margin: 5% 15%;
							}

							.details{
								margin: 5% 15%;

							}

							.yachts{
								margin: auto 20%;
							}

							.yachts img{
								width: 100%;
							}

							.separator{
								color: #000;
								background-color: #000;
								height: 5px;
							}

							.options{
								width: 90%;
							}

							.bold{
								font-weight: bold;
							}

						</style>
					</head>
					<body>
						<div class=\"logo\">
							<img src=\"http://my.saycarbon.com/assets/img/logopdf.png\">
						</div>
						<div class=\"subtitle\">
							HEY $titulo $apellido<br>
							WELL DONE
						</div>
						<div class=\"title\">
							HERE IS YOUR<br>
							$yacht CARBON!
						</div>
						<div class=\"summary\">
							<p style=\"text-align: left; font-weight: bold;\">
								SUMMARY
								<span style=\"float: right;\">
									$yacht Carbon - TOTAL AMOUNT $price €
								</span>
							</p>
							<hr class=\"separator\">
							<div class=\"options\">
								<p class=\"bold\">ENGINE: $engine</p>
								<p class=\"bold\">HULL COLOR: $colour</p>
								<p class=\"bold\">FLOOR: $floor</p>
								<p class=\"bold\">UPHOLSTERY: $upholstery</p>
								<p class=\"bold\">ADD-ONS: $addon</p>
							</div>
						</div>
						<div class=\"details\">
							<p style=\"text-align: left; font-weight: bold;\">
								YOUR DETAILS
							</p>
							<hr class=\"separator\">
							<div class=\"options\">
								<p class=\"bold\">$nombre</p>
								<p class=\"bold\">$apellido</p>
								<p class=\"bold\">$ciudad</p>
								<p class=\"bold\">$pais</p>
								<p class=\"bold\">$correo</p>
								<p class=\"bold\">$telefono</p>
							</div>
						</div>

						<div class=\"yachts\">
							<img src=\"http://my.saycarbon.com/assets/img/$data1\">
							<img src=\"http://my.saycarbon.com/assets/img/$data2\">
							<img src=\"http://my.saycarbon.com/assets/img/$data3\">
						</div>
					</body>
				</html>";
			$dompdf->load_html($html);
			$dompdf->render();
			$output = $dompdf->output();
			file_put_contents(strtolower($url).'.pdf', $output);
			$infoyacht = array(
				'yacht' => $yacht,
				'engine' => $engine,
				'colourscheme' => $colour,
				'upholstery' => $upholstery,
				'floor' => $floor,
				'addons' => $addon,
				'emailCustomer' => $correo,
				'price' => $price
			);

		}else{

			$this->load->library('pdf');
			$dompdf = new PDF();
			$dompdf->setPaper('A4', 'portrait');
			$dompdf->set_option('isRemoteEnabled', TRUE);
			ob_start();
			$html = "<html>
						<head>
							<meta charset=\"UTF-8\"/>
							<title>SAY Yachts</title>
							<style type=\"text/css\">
								@font-face {
					    				font-family: Watchword;
					    				src: url(\"/assets/fonts/watchword.otf\");
								}

								@font-face {
					    				font-family: GillSans;
					    				src: url(\"/assets/fonts/watchword.otf\");
								}

								h2{
									color: blue;
								}

								.logo{
									text-align: right;
									margin: 1% 3%;
								}

								.logo img{
									width: 15%;
									height: 5%;
								}

								.subtitle{
									margin-top: 60px;
									text-align: center;
									font-size: 20px;
									letter-spacing: 2px;
									font-family: Watchword;
									font-weight: lighter;
								}

								.title{
									text-align: center;
									margin: 30px;
									font-size: 30px;
									font-family: GillSans;
								}

								.summary{
									margin: 5% 15%;
									height: 35%;
								}

								.details{
									margin: 15% 15%;
								}

								.yachts{
									margin: auto 20%;
								}

								.yachts img{
									width: 100%;
								}

								.separator{
									color: #000;
									background-color: #000;
									height: 5px;
								}

								.options{
									width: 90%;
									height 100%;
								}

								.bold{
									font-weight: bold;
								}

							</style>
						</head>
						<body>
							<div class=\"logo\">
								<img src=\"http://my.saycarbon.com/assets/img/logopdf.png\">
							</div>
							<div class=\"subtitle\">
								HEY $titulo $apellido<br>
								WELL DONE
							</div>
							<div class=\"title\">
								HERE IS YOUR<br>
								$yacht!
							</div>
							<div class=\"summary\">
								<p style=\"text-align: left; font-weight: bold;\">
									SUMMARY
									<span style=\"float: right;\">
										$yacht - TOTAL AMOUNT $price €
									</span>
								</p>
								<hr class=\"separator\">
								<div class=\"options\">
									<h3>INCLUDES</h3>
										<p class=\"bold\">ENGINE: 2x V8 Volvo Penta 6,2 Ltr. 430HP, Z-Drive, Aquamatic Duoprop. </p>
										<p class=\"bold\">Addons: Natural certified Teak decking. </p>
										<p class=\"bold\">Bimini top with Carbon poles. </p>
										<p class=\"bold\">Visible Carbon package. </p>
										<p class=\"bold\">Custom color for soft surfaces. </p>
										<p class=\"bold\">Custom uni color line. </p>
										<p class=\"bold\">Custom RAL color on tube. </p>
										<p class=\"bold\">LED Underwater lighting under swim platform. </p>
										<p class=\"bold\">Electric swim ladder with hand rail. </p>
										<p class=\"bold\">Bow Thruster. </p>
										<p class=\"bold\">Garmin VHF 300i radio. </p>
										<p class=\"bold\">LED engine room lightning. </p>
										<p class=\"bold\">Mooring kit: 2 small + 4 large fenders, 4 lines. </p>
										<p class=\"bold\">Anti fouling. </p>
										<p class=\"bold\">Tailored cockpit persenning. </p>
										<p class=\"bold\">CE Category B Certification incl. necessary modifications. </p>
										<p class=\"bold\">Waterski set. </p>
									</p>
								</div>
							</div>
							<div class=\"details\">
								<p style=\"text-align: left; font-weight: bold;\">
									YOUR DETAILS
								</p>
								<hr class=\"separator\">
								<div class=\"options\">
									<p class=\"bold\">$nombre</p>
									<p class=\"bold\">$apellido</p>
									<p class=\"bold\">$ciudad</p>
									<p class=\"bold\">$pais</p>
									<p class=\"bold\">$correo</p>
									<p class=\"bold\">$telefono</p>
								</div>
							</div>

							<div class=\"yachts\">
								<img src=\"http://my.saycarbon.com/assets/img/".strtolower($url)."_back/1.png\">
								<img src=\"http://my.saycarbon.com/assets/img/".strtolower($url)."_front/1.png\">
								<img src=\"http://my.saycarbon.com/assets/img/".strtolower($url)."_top/1.png\">
							</div>
						</body>
					</html>";
				$dompdf->load_html($html);
				$dompdf->render();
				$output = $dompdf->output();
				file_put_contents(strtolower($url).'.pdf', $output);

				$infoyacht = array(
					'yacht' => $yacht,
					'emailCustomer' => $correo,
					'price' => $price
				);

				$this->db->insert('order_yacht', $infoyacht);
		}

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);
		$this->email->from('welcome@saycarbon.com');
		$this->email->set_newline("\r\n");
		$this->email->to($correo);
		$this->email->cc('welcome@saycarbon.com');
		$this->email->subject('Say Yachts'); // email Subject
        $this->email->message('Hello, '.$titulo.' '.$apellido.','."\n".'nice to e-meet you! Thank you for you interest.'."\n".'See attached your customised SAY Carbon Yachts.'."\n".'Contact us for more details: madeleine.schaldach@say-yacht.com'."\n"."\n"."\n".'Kind regards,'."\n".'Madeleine Schaldach'."\n".'Management Assistant'."\n".'Hatternholzweg 13'."\n".'88239 Wangen i. A. / Germany'."\n".'Tel.:     +49 7520 96990-16'."\n".'E-Mail:   madeleine.schaldach@say-yacht.com'."\n".'Web:      www.say-yacht.com');

        $this->email->attach(strtolower($url).'.pdf');
        $this->email->attach(base_url().'/assets/img/logo_say_2018_rgb.png', "inline");
        $this->email->send();

        $header['title'] = 'SAY Yachts - Successful';
        $header['logo_say'] = base_url().'assets/img/logo_say.png';
		$header['css'] = base_url().'assets/css/order.css';
		$body['js'] = base_url().'assets/js/customize.js';
		if($url == "say45"){
			$body['yacht'] = base_url().'assets/img/'.strtolower($url).'_back/1.png';
		}else if($url == "say29"){
			$body['yacht'] = base_url().'assets/img/'.strtolower($url).'_back/'.$numtapi.'/'.$numcolor.'.png';
		}else{
			$body['yacht'] = base_url().'assets/img/'.strtolower($url).'_back/'.$roof.'/'.$numtapi.'/'.$numcolor.'.png';
		}
		$this->load->view('index/header', $header);
		$this->load->view('order', $body);
	}
}
