document.querySelectorAll('.menu').forEach(btn => {
    btn.addEventListener('click', e => {
        btn.classList.toggle('active');
    });
});

var step=0, number2=-1, precioinicial=198000, preciomotor, preciocolor, precioteca, preciotecho, precioaddons=0, preciofinal, fuel, numcolor=0, color, numtapi=1, numtecho = 1; tapi="DIAMANTE IVORY", teca="TEAK", roof="ROOF1", addons = [];

$(window).on("load",function(){
    $(".loader-wrapper").delay(2000).animate({width: '0px', left: '-20px'}, 800);
});

//Funcion cambio de color y tapiceria:

function changeColorBack(a, b, c) {
    return "/assets/img/say29_back/"+a+"/"+c+"/"+b+".png"
};

function changeColorFront(a, b, c) {
    return "/assets/img/say29_front/"+a+"/"+c+"/"+b+".png"
};

function changeColorTop(a, b, c) {
    return "/assets/img/say29_top/"+a+"/"+c+"/"+b+".png"
};

function breathSubtitle(text){
  $(".subtitle").animate({fontSize: '+=5rem!important'}, 500);
  $(".subtitle").text(text);
  $(".subtitle").animate({fontSize: '-=5rem!important'}, 500);
}

function breath(){
  $(".content").fadeOut();
  $(".content").fadeIn();
  $(".summary").fadeOut();
  $(".summary").fadeIn();
}

function extrasSelected(){
  addons = [];
  $('.addons').text("");
  $(".btn-success").each(function(){
    var id = $(this).attr('id');
    var text = $("#"+id).text();
    addons.push(text);
    $('.addons').append("Extra: "+text+"<br>");
  });

  $('.summary').animate({height: "100vh"});
  console.log(addons);
}

function extraSelected(){
  $('.addons').text("");
  $(".btn-success").each(function(){
    var id = $(this).attr('id');
    var text = $("#"+id).text();
    addons.push(text);
    $('#subtitle').append("<br> Extra: "+text);
  });
}

function addValues(numero){
  var addon = "";
  for (var i=0; i<addons.length; i++) { 
    addon += addons[i]+' <br>';
  }

  console.log(addon);
  $('#yachtform').val('SAY 29');
  $('#engineform').val(fuel);
  $('#colourform').val(color);
  $('#upholsteryform').val(tapi);
  $('#floorform').val(teca);
  $('#addonsform').val(addon);
  $('#priceform').val(numero);
  $('#numcolor').val(numcolor);
  $('#numtapi').val(numtapi);

}

$(document).ready(

	function(){
    $(".price").text(precioinicial.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    $('#botonMenu').slideUp();
    $('.masthead').animate({"opacity": 1},1000);
    //$('.carousel').slideUp("fast");
    $('.option-group-color').slideUp("fast");
    $('.option-color').slideUp("fast");
    $('.option-tapiceria').slideUp("fast");
    $('.option-teca').slideUp("fast");
    $('.option-roof').slideUp("fast");
    $('.formulario').slideUp("fast");
    $('.option-addons').slideUp("fast");
    $('#add').slideUp("fast");
    $('#comfort').slideUp("fast");
    $('#hard').slideUp("fast");
    $('#acce').slideUp("fast");
    $('.btn-next').attr("disabled", true);
    $('.btn-nextmobile').attr("disabled", true);


    $("#down").click(function(){
        var y = $(".option-text").scrollTop();
        $('.option-text').animate({scrollTop: y + 500 });
        console.log(y);
     });

    $("#up").click(function(){
        var y = $(".option-text").scrollTop();
        $('.option-text').animate({scrollTop: y - 500 });
        console.log(y);
     });


    $(".btn-cerrar").click(function(){
      $('.lateral').animate({"right": -900});
      $('.main').animate({left: '0vw'});
      $('.main').animate({width: '100vw'});
      $('.customize').animate({left: '7vw'});
      $('.customize').animate({width: '100vw'});
      $('.options').addClass('invisible');
      $('.option-overlay').animate({left: '-200px'});
      $('.titulo').addClass('invisible');
      if(step==2){
        $('.option-teca').slideUp("fast");
      }
    });

    $(".btn-more").click(function(){
      $('.lateral').animate({"right": 0});
      $('.main').animate({left: '-4vw'});
      $('.main').animate({width: '70vw'});
      $('.customize').animate({left: '-4vw'});
      $('.customize').animate({width: '70vw'});
      $('.options').removeClass('invisible');
      $('.option-overlay').animate({left: '-400px'});
      $('.titulo').removeClass('invisible');
      if(step==2){
        $('.option-teca').slideDown("fast");
      }
    });


    //Menú derecho
    $('.btn-next').click(function(){
      switch(step){
        //COLOR UPHOLSTERY
        case 0: $('.btn-back').removeClass("disabled");
                $('.btn-backmobile').removeClass("disabled");
                $('.btn-next').attr("disabled", true);
                $('.option-fuel').slideUp(500);
                $('.option-group-color').delay(500).slideDown(500);
                $('.option-color').delay(500).slideDown(1);
                $('.option-tapiceria').delay(500).slideDown(1);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.").append("<br> <br> We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)");
                breathSubtitle("SAY29: HULL & UPHOLSTERY");
                breath();
                upDownColor(0);
                break;
        //FLOOR
        case 1: $('.btn-back').removeClass("disabled");
                $('.btn-backmobile').removeClass("disabled");
                $('.btn-next').attr("disabled", true);
                $('.option-group-color').slideUp(1000)
                $('.option-color').slideUp(1000);
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.color').text("Colourscheme: "+color);
                $('.tapi').text("Upholstery: "+tapi);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.");
                breathSubtitle("SAY29: FLOOR");
                breath();
                break;

        //ROOF
        case 2: $('.btn-back').removeClass("disabled");
                $('.btn-backmobile').removeClass("disabled");
                $('.btn-next').attr("disabled", true);
                $('.option-group-color').slideUp(1000)
                $('.option-color').slideUp(1000);
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.option-roof').delay(1000).slideDown(1000);
                $('.teca').text("Floor: "+teca);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Choose your roof.");
                breathSubtitle("SAY29: ROOF");
                breath();
                break;
        //OPTIONS
        case 3: $('.btn-next').attr("disabled", false);
                $('.btn-nextmobile').attr("disabled", false);
                $('#myCarousel').fadeOut().slideUp();
                $('.option-roof').slideUp(1000);
                $('.option-addons').delay(1000).slideDown(1000);
                $('#add').slideDown(500);
                $('.roof').text("Roof: "+roof);
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                breathSubtitle("SAY29: OPTIONS");
                breath();
                break;
        //SUMMARY
        case 4: $('.btn-next').attr("disabled", false);
                $('.btn-nextmobile').attr("disabled", false);
                $('.option-addons').slideUp();
                $('.content').slideUp("fast");
                $('#myCarousel').delay(400).slideDown();
                $('.formulario').delay(400).slideDown();
                $('.btn-next').text("SEND NOW");
                $('.btn-next').addClass('invisible');
                breathSubtitle("SAY29: SUMMARY");
                extrasSelected();
                break;
      }
      step++;
    });

    $('.btn-nextmobile').click(function(){
      switch(step){
        //COLOR
        case 0: $('.btn-backmobile').removeClass("disabled");
                $('.btn-nextmobile').attr("disabled", true);
                $('.option-fuel').slideUp(500);
                $('.option-group-color').delay(500).slideDown(500);
                $('.option-color').delay(500).slideDown(1);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.");
                breathSubtitle("SAY29: HULL & UPHOLSTERY");
                breath();
                upDownColor(0);
                break;
        //UPHOLSTERY
        case 1: $('.btn-backmobile').removeClass("disabled");
                $('.btn-nextmobile').attr("disabled", true);
                $('.option-group-color').slideUp(1000)
                $('.option-color').slideUp(1000);
                $('.option-tapiceria').delay(1000).slideDown(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.color').text("Colourscheme: "+color);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.");
                breathSubtitle("SAY29: FLOOR");
                breath();
                break;
        //EXTRAS
        case 2: $('.btn-nextmobile').attr("disabled", false);
                $('#myCarousel').fadeOut().slideUp();
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.option-addons').delay(1000).slideDown(1000);
                $('#add').slideDown(500);
                $(".opt-2").slideUp("fast");
                $(".opt-3").slideUp("fast");
                $(".opt-4").slideUp("fast");
                $('.tapi').text("Upholstery: "+tapi);
                $('.teca').text("Floor: "+teca);
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                breathSubtitle("SAY29: ROOF");
                breath();
                break;
        //SUMMARY
        case 3: $('.btn-nextmobile').attr("disabled", false);
                $('.option-addons').slideUp();
                $('.content').slideUp("fast");
                $('#myCarousel').delay(400).slideDown();
                $('.formulario').delay(400).slideDown();
                $('.btn-nextmobile').text("SEND NOW");
                $('.btn-nextmobile').addClass('invisible');
                breathSubtitle("SAY29: SUMMARY");
                extrasSelected();
                break;
      }
      step++;
    });

    $('.btn-back').click(function(){
      step--;
      switch(step){
        //ENGINE
        case 0: $('.btn-back').attr("disabled", true);
                $('.btn-next').attr("disabled", true);
                $('.option-color').slideUp(500);
                $('.option-group-color').slideUp(500);
                $('.option-fuel').delay(500).slideDown(500);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("The hull made of pure carbon composite weighs just 380 kg. This low weight allows for sports car-like acceleration, even with a 350 horsepower engine. With the 483 hp-strong top engine (Ilmor MV8) the SAY29 Runabout can even reach maximum speeds of up to 52 knots (96km/h).");
                breathSubtitle("SAY29: ENGINE");
                breath();
                upDownColor(0);
                break;
        //COLOR
        case 1: $('.btn-next').attr("disabled", true);
                $('.option-group-color').delay(1000).slideDown(1000)
                $('.option-color').delay(1000).slideDown(1000);
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.color').text("Colourscheme: "+color);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.");
                breathSubtitle("SAY29: COLOURSCHEME");
                breath();
                break;
        //UPHOLSTERY
        case 2: $('.btn-next').attr("disabled", false);
        $('.btn-nextmobile').attr("disabled", false);
                $('#myCarousel').delay(1000).fadeIn().slideDown();
                $('.option-addons').slideUp(1000);
                $('.option-tapiceria').delay(1000).slideDown(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.tapi').text("Upholstery: "+tapi);
                $('.teca').text("Floor: "+teca);
                $("#title").text(tapi+" + "+teca);
                $("#subtitle").text("");
                breath();
                break;
        //EXTRAS
        case 3: $('.btn-next').attr("disabled", false);
        $('.btn-nextmobile').attr("disabled", false);
                $('.option-addons').delay(400).slideDown();
                $('.content').slideDown(400);
                $('#myCarousel').slideUp();
                $('.formulario').slideUp();
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                $('.btn-next').text("NEXT");
                $('.btn-next').removeClass('invisible');
                $('.summary').css({height: ""});
                breathSubtitle("SAY29: OPTIONS");
                breath();
                break;
      }
    });

    $('.btn-backmobile').click(function(){
      step--;
      switch(step){
        //ENGINE
        case 0: $('.btn-backmobile').attr("disabled", true);
                $('.option-color').slideUp(500);
                $('.option-group-color').slideUp(500);
                $('.option-fuel').delay(500).slideDown(500);
                $('.fuel').text("Engine: "+fuel);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("The hull made of pure carbon composite weighs just 380 kg. This low weight allows for sports car-like acceleration, even with a 350 horsepower engine. With the 483 hp-strong top engine (Ilmor MV8) the SAY29 Runabout can even reach maximum speeds of up to 52 knots (96km/h).");
                breathSubtitle("SAY29: ENGINE");
                breath();
                upDownColor(0);
                break;
        //COLOR
        case 1: $('.btn-nextmobile').attr("disabled", true);
                $('.option-group-color').delay(1000).slideDown(1000)
                $('.option-color').delay(1000).slideDown(1000);
                $('.option-tapiceria').slideUp(1000);
                $('.option-teca').slideUp(1000);
                $('.color').text("Colourscheme: "+color);
                $("#title").text("SAY 29 Carbon");
                $("#subtitle").text("Customize your SAY Carbon yacht and be the eyecatcher while cruising in style!").append("<br> <br> Here you can choose the colour scheme of the hull.");
                breathSubtitle("SAY29: COLOURSCHEME");
                breath();
                break;
        //UPHOLSTERY
        case 2: $('.btn-nextmobile').attr("disabled", false);
                $('#myCarousel').delay(1000).fadeIn().slideDown();
                $('.option-addons').slideUp(1000);
                $('.option-tapiceria').delay(1000).slideDown(1000);
                $('.option-teca').delay(1000).slideDown(1000);
                $('.tapi').text("Upholstery: "+tapi);
                $('.teca').text("Floor: "+teca);
                $("#title").text(tapi+" + "+teca);
                $("#subtitle").text("");
                breath();
                break;
        //EXTRAS
        case 3: $('.btn-nextmobile').attr("disabled", false);
                $('.option-addons').delay(400).slideDown();
                $('.content').slideDown(400);
                $('#myCarousel').slideUp();
                $('.formulario').slideUp();
                $("#title").text("EXTRAS");
                $("#subtitle").text("");
                $('.btn-nextmobile').text("NEXT");
                $('.btn-nextmobile').removeClass('invisible');
                $('.summary').css({height: ""});
                breathSubtitle("SAY29: OPTIONS");
                breath();
                break;
      }
    });

    // OPCIONES FUEL
		$("#option-fuel1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      fuel = "Mercruiser V8";
      preciomotor = 38750.00;
      $("#title").text(fuel);
      $("#subtitle").text('6.2l 350hp mpi, incl. Bravo3 drive').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
		});

    $("#option-fuel2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      fuel = "Mercruiser V8";
      $("#title").text(fuel);
      preciomotor = 41620.00;
      $("#subtitle").text('6.2l 350hp mpi, sea core, incl. Bravo3 drive').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

    $("#option-fuel3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      fuel = "Volvo Penta V8-430-CE DPS";
      $("#title").text(fuel);
      preciomotor = 47906.00;
      $("#subtitle").text('6.2l 430hp including Aquamatic duo-prop drive').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

    $("#option-fuel4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      fuel = "Ilmor MV8 7.4l";
      $("#title").text(fuel);
      preciomotor = 50925.00;
      $("#subtitle").text('483hp, incl. Ilmor One Drive').append('<br><br>+'+preciomotor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

    $("#option-fuel5").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      fuel = "Kreisel Performance E-Drive";
      $("#title").text(fuel);
      preciomotor = 192460.00;
      $("#subtitle").text('Award winning SAY29E Runabout Carbon (German Innovation Award 2019 for E-Mobility) is equipped with the latest and most efficient propulsion system from KREISEL Electric. ').append('<br> <br> Kreisel Performance E-Drive incl. 120kWh battery capacity. <br><br>+'+preciomotor.toLocaleString()+' € <i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });





    //OPCIONES GRUPO COLOR

    $("#option-group-color1").click(function(){
      $("#title").text("METALLIC COLLECTION");
      $("#subtitle").text("Enjoy mesmerizing recflections of turqouise waters in Ibiza thanks to your glossy metallic painted hull.");
      upDownColor(0);
    });

    $("#option-group-color2").click(function(){
      $("#title").text("SAY LIFESTYLE");
      $("#subtitle").text("");
      upDownColor(1);
    });

    $("#option-group-color3").click(function(){
      $("#title").text("PURE COLLECTION");
      $("#subtitle").text("For puristic design lovers.");
      upDownColor(2);
    });

    $("#option-group-color4").click(function(){
      $("#title").text("RACING COLLECTION");
      $("#subtitle").text("Who says racing colours only exist on our roads?");
      upDownColor(3);
    });


    //OPCIONES COLOR
    $("#option-color1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "CANNES (Champagne)";
      $("#title").text('CANNES (Champagne)');
      preciocolor = 8360.50;
      $("#subtitle").text('Shiny like a golden sunset in Cannes. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 1;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "MILAN (Platinum)";
      $("#title").text('MILAN (Platinum)');
      preciocolor = 8360.50;
      $("#subtitle").text('Timeless and luxurious. Always an exquisite choice. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 2;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "IBIZA (Blue)";
      $("#title").text('IBIZA (Blue)');
      preciocolor = 8360.50;
      $("#subtitle").text('Perfect interaction with the crytal clear waters in Ibiza and Formentera. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 3;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "OCEAN (Royal Blue)";
      $("#title").text('OCEAN (Royal Blue)');
      preciocolor = 8360.50;
      $("#subtitle").text('Pure and timesless elegance. ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 4;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color5").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "CARRARA BLUE";
      $("#title").text('CARRARA BLUE');
      preciocolor = 0;
      $("#subtitle").text('A delicate tone of blue to mark the difference.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 5;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color6").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "AMBITIOUS GREEN";
      $("#title").text('AMBITIOUS GREEN');
            preciocolor = 0;
      $("#subtitle").text('Perfect match to your distinctive life-style.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 6;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color7").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "SKY BLUE";
      $("#title").text('SKY BLUE');
      preciocolor = 0;
      $("#subtitle").text('This happy feeling when you look into the blue endless sky... ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 7;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color8").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "WHITE";
      $("#title").text('WHITE');
      preciocolor = 0;
      $("#subtitle").text('100% Purity: The positive and perfect classic.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 8;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color9").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "CRAYON GREY";
      $("#title").text('CRAYON GREY');
      preciocolor = 0;
      $("#subtitle").text('Our favourite all-rounder. Pure Elegance.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 9;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color10").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "ANTHRACITE";
      $("#title").text('ANTHRACITE');
      preciocolor = 0;
      $("#subtitle").text('Modern. Sophisticated. Timeless!').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 10;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color11").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "DYNAMIC ORANGE";
      $("#title").text('DYNAMIC ORANGE');
      preciocolor = 0;
      $("#subtitle").text('Full of energy. Full of motivation. Let’s go yachting!').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 11;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color12").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "RACING GREEN";
      $("#title").text('RACING GREEN');
      preciocolor = 0;
      $("#subtitle").text('This colour on your speed cruiser promises what it stands for.').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 12;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-color13").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      color = "MIAMI BLUE";
      $("#title").text('MIAMI BLUE');
      preciocolor = 0;
      $("#subtitle").text('The colour to cheer you up! Who fancy being the eye catcher? ').append('<br><br>+'+preciocolor.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      numcolor = 13;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });





    //OPCIONES TAPICERIA
    $("#option-tapiceria1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "DIAMANTE IVORY";
      $("#title").text(tapi);
      numtapi = 1;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "HITCH CERULEAN";
      $("#title").text(tapi);
      numtapi = 2;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "DIAMANTE SAGE";
      $("#title").text(tapi);
      numtapi = 3;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria4").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "SILVERTEX PLATA";
      $("#title").text(tapi);
      numtapi = 4;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria5").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "SILVERTEX ICE";
      $("#title").text(tapi);
      numtapi = 5;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria6").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "SILVERTEX TAUPE";
      $("#title").text(tapi);
      numtapi = 6;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria7").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "DIAMANTE TONIC";
      $("#title").text(tapi);
      numtapi = 7;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });

    $("#option-tapiceria8").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      tapi = "DIAMANTE CARBON";
      $("#title").text(tapi);
      numtapi = 8;
      $('.yacht1').attr("src", changeColorBack(numtecho,numcolor,numtapi));
      $('.yacht2').attr("src", changeColorFront(numtecho,numcolor,numtapi));
      $('.yacht3').attr("src", changeColorTop(numtecho,numcolor,numtapi));
    });





    //OPCIONES TECA
    $("#option-teca1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      teca = "TEAK";
      $("#title").text(teca); 
      precioteca = 1217.64;
      $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.").append('<br><br>+'+precioteca.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    });

     $("#option-teca2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      teca = "SEATEK";
      $("#title").text(teca);
      precioteca = 3604.20;
      $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.").append('<br><br>+'+precioteca.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
     });

     $("#option-teca3").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      teca = "FOAM";
      $("#title").text(teca);
      precioteca = 3604.20;
      $("#subtitle").text("We use high quality upholstery material with the latest coating solutions on the market (Silverguard ®/ Permablok3®)").append("<br> <hr> Choose between natural certified Teak decking and SeaTek Floor Options formulated with a 3M™ acrylic based high-bond pressure sensitive adhesive,that are easy to clean and stain resistant.").append('<br><br>+'+precioteca.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
      $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
     });

     $("#option-roof1").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
       roof='ROOF1';
       numtecho = 1;
       preciotecho = 28192.44;

       $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
       $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
       $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
       $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
       $("#subtitle").text("");
       $("#subtitle").append(preciotecho.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#option-roof2").click(function(){
      $('#principal').animate({top: '40'}).animate({top: '0'});
      $('.btn-next').attr("disabled", false);
      $('.btn-nextmobile').attr("disabled", false);
      roof='ROOF2';
       numtecho = 2;
       preciotecho = 38958.44;

       $('.yacht1').attr("src", changeColorBack(numtecho, numcolor,numtapi));
       $('.yacht2').attr("src", changeColorFront(numtecho, numcolor,numtapi));
       $('.yacht3').attr("src", changeColorTop(numtecho, numcolor,numtapi));
       $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
       $("#subtitle").text("");
       $("#subtitle").append(preciotecho.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });


     //OPCION OVERLAY
     $(".opt-1").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#2").css("background", "#494949");
        $("#add").css("background", "#5B5B5B");
        $("#hard").slideDown(500);
        $("#comfort").slideUp(500);
        $("#acce").slideUp(500);
        $(".opt-1").slideUp(1);
        $(".opt-2").slideDown(500);
        $(".opt-3").slideDown(500);
        $(".opt-4").slideDown(500);
     });

     $(".opt-2").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideDown(500);
        $("#2").css("background", "#494949");
        $("#hard").css("background", "#5B5B5B");
        $("#comfort").slideUp(500);
        $("#acce").slideUp(500);
     });

     $(".opt-3").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideUp(500);
        $("#comfort").slideDown(500);
        $("#3").css("background", "#E8E8E8");
        $("#comfort").css("background", "#5B5B5B");
        $("#acce").slideUp(500);
     });

     $(".opt-4").click(function(){
        $(".option-text").fadeOut(1);
        $(".option-text").delay(500).fadeIn(1);
        $("#add").slideUp(500);
        $("#hard").slideUp(500);
        $("#comfort").slideUp(500);
        $("#acce").slideDown(500);
        $("#4").css("background", "#494949");
        $("#acce").css("background", "#5B5B5B");
     });

     $("#hard1").click(function(){
      $("#hard1").toggleClass("btn-light").toggleClass("btn-success");
        if ($("#hard1").hasClass("btn-success")) {
          precioaddons += 1217.64;
        }else{
          precioaddons -= 1217.64;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1217.64.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#hard2").click(function(){
      $("#hard2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#hard2").hasClass("btn-success")) {
          precioaddons += 5755.45;
        }else{
          precioaddons -= 5755.45;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(5755.45.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#hard3").click(function(){
      $("#hard3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#hard3").hasClass("btn-success")) {
          precioaddons += 301.68;
        }else{
          precioaddons -= 301.68;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(301.68.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#hard4").click(function(){
      $("#hard4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#hard4").hasClass("btn-success")) {
          precioaddons += 1824.00;
        }else{
          precioaddons -= 1824.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1824.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#hard5").click(function(){
      $("#hard5").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#hard5").hasClass("btn-success")) {
          precioaddons += 805.88;
        }else{
          precioaddons -= 805.88;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(805.88.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort1").click(function(){
      $("#comfort1").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort1").hasClass("btn-success")) {
          precioaddons += 1611.76;
        }else{
          precioaddons -= 1611.76;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1611.76.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort2").click(function(){
      $("#comfort2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort2").hasClass("btn-success")) {
          precioaddons += 1233.20;
        }else{
          precioaddons -= 1233.20;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1233.20.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort3").click(function(){
      $("#comfort3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort3").hasClass("btn-success")) {
          precioaddons += 960.20;
        }else{
          precioaddons -= 960.20;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(960.20.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort4").click(function(){
      $("#comfort4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort4").hasClass("btn-success")) {
          precioaddons += 6007.56;
        }else{
          precioaddons -= 6007.56;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(6007.56.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort5").click(function(){
      $("#comfort5").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort5").hasClass("btn-success")) {
          precioaddons += 2850.60;
        }else{
          precioaddons -= 2850.60;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(2850.60.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort6").click(function(){
      $("#comfort6").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort6").hasClass("btn-success")) {
          precioaddons += 1805.89;
        }else{
          precioaddons -= 1805.89;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1805.89.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort7").click(function(){
      $("#comfort7").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort7").hasClass("btn-success")) {
          precioaddons += 12520.17;
        }else{
          precioaddons -= 12520.17;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(12520.17.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>')
        extraSelected();;
     });

     $("#comfort8").click(function(){
      $("#comfort8").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort8").hasClass("btn-success")) {
          precioaddons += 167.23;
        }else{
          precioaddons -= 167.23;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(167.23.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort9").click(function(){
      $("#comfort9").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort9").hasClass("btn-success")) {
          precioaddons += 410.92;
        }else{
          precioaddons -= 410.92;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(410.92.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort10").click(function(){
      $("#comfort10").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort10").hasClass("btn-success")) {
          precioaddons += 4260.30;
        }else{
          precioaddons -= 4260.30;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(4260.30.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort11").click(function(){
      $("#comfort11").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort11").hasClass("btn-success")) {
          precioaddons += 411.76;
        }else{
          precioaddons -= 411.76;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(411.76.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#comfort12").click(function(){
      $("#comfort12").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#comfort12").hasClass("btn-success")) {
          precioaddons += 1436.00;
        }else{
          precioaddons -= 1436.00;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1436.00.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce1").click(function(){
      $("#acce1").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce1").hasClass("btn-success")) {
          precioaddons += 587.40;
        }else{
          precioaddons -= 587.40;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(587.40.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce2").click(function(){
      $("#acce2").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce2").hasClass("btn-success")) {
          precioaddons += 1763.87;
        }else{
          precioaddons -= 1763.87;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(1763.87.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce3").click(function(){
      $("#acce3").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce3").hasClass("btn-success")) {
          precioaddons += 2378.81;
        }else{
          precioaddons -= 2378.81;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(2378.81.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce4").click(function(){
      $("#acce4").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce4").hasClass("btn-success")) {
          precioaddons += 839.50;
        }else{
          precioaddons -= 839.50;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(839.50.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $("#acce5").click(function(){
      $("#acce5").toggleClass("btn-light").toggleClass("btn-success");
      if ($("#acce5").hasClass("btn-success")) {
          precioaddons += 460.20;
        }else{
          precioaddons -= 460.20;
        }
        $(".price").text((precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho).toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
        $("#subtitle").text("");
        $("#subtitle").append(460.20.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i><br>');
        extraSelected();
     });

     $('.btn-order').click(function(){
      preciofinal = (precioinicial+preciomotor+preciocolor+precioteca+precioaddons+preciotecho);
      addValues(preciofinal);
     });
  }   
);