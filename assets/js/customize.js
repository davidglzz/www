$(window).on("load",function(){
    $(".loader-wrapper").delay(1000).animate({width: '0px', left: '-10px'}, 800);
});

$(document).ready(
	function(){
    $('#botonMenu').slideUp();
    $('.masthead').animate({"opacity": 1},1000);
    $('.card').animate({"top": 50}, 600).animate({"top": 0}, 250);

		$(".btn-more").click(function(){
      $(".btn-more").toggleClass("active");
      if($(".btn-more").hasClass("active")){
        $('.say29').fadeOut();
        $('.say42').fadeOut();
        $('.say45').delay(400).fadeIn();
      }else{
        $('.say45').fadeOut();
        $('.say29').delay(400).fadeIn();
        $('.say42').delay(400).fadeIn();
      }
		});

    $(".say29").click(function(){
      $('.lateral').animate({"right": 0});
      $('.title-lateral').text("SAY 29 Carbon");
      $('.subtitle-lateral').text("SAY Carbon Yachts demonstrating how we can reduce our carbon footprint and remain with a stylish, modern and luxury boating lifestyle without restricting performance.").
      append("<br><br> The combination of the light-weight Carbon hydrodynamic hull and Volvo Penta 430HP low-emission engine excels for breath-taking acceleration to top speeds of 45 knots! <br><br> Every detail on the SAY 29 Carbon is optimized for speed and agility, all components are perfectly matched. The custom wave cutting bow design combined with stability side wings on the bathing platform ensures for maximum steadiness while enabling incredible cornering speeds. <br><br> As well, everybody knows that ELECTRIC is the present. At SAY Carbon Yachts, we know that too. <br><br> The innovative technology provided by the e-mobility pioneers in Europe – Kreisel Electric – provides the 'SAY29E' with a cruising range of approx. 30 NM at 22 knots. The charging concept includes a type 2 charging connection with a 22kW on-board charger, which fully recharges the battery in approx. 6 hours. <br><br> Are you ready to customise your SAY29?");
      $('.btn-customize').addClass('btn-29');
      $('.btn-customize').removeClass('btn-42');
      $('.btn-customize').removeClass('btn-45');
    });

    $(".say42").click(function(){
      $('.lateral').animate({"right": 0});
      $('.title-lateral').text("SAY 42 Carbon");
      $('.subtitle-lateral').text("The new SAY 42 Carbon is another breakthrough of low-emission cruising with high-performance handling. As with the successful SAY 29 Carbon she provides revolutionary hydrodynamic lightweight hull design enabling speeds above 50 knots. ").
      append("<br><br> A spacious cabin, available with a choice of personalised luxury materials and styling includes a generous double berth and complemented bathroom equipped with the latest technology, including the revolutionary Seakeeper 2 GYRO, the SAY 42R Carbon offers spacious cockpit seating incorporating storage space for all your necessary water toys. <br><br> The unexpected large stabilizing bathing platform contains an electric hydraulic bathing ladder/Passerelle for elegant boarding.");
      $('.btn-customize').addClass('btn-42');
      $('.btn-customize').removeClass('btn-29');
      $('.btn-customize').removeClass('btn-45');
    });

    $(".say45").click(function(){
      $('.lateral').animate({"right": 0});
      $('.title-lateral').text("SAY 45 RIB");
      $('.subtitle-lateral').text("RIBs, or rigid-inflatable boats, are lightweight, easy-to-steer and very safe. The combination of the tried and tested RIB design and our ultra-light carbon hull redefines performance in this type of boat. At 13.70 metres long, the SAY45 RIB is very spacious and approved for up to 14 passengers. ").
      append("<br><br> Up to 1250 horsepower paired with this boat’s light weight guarantees impressive acceleration. With its characteristic, sharp wavecutter bow, the SAY45 RIB has no problem cutting through every wave and reaching top speeds of up to 60 knots (115 km/h). The D-shaped air tube allows impressively easy handling in the port and maximum safety on the open water. When fitted with a jet drive, the low hull draught makes the SAY45 RIB perfect for shallow water.");
      $('.btn-customize').addClass('btn-45');
      $('.btn-customize').removeClass('btn-42');
      $('.btn-customize').removeClass('btn-29');
    });

    $("#down").click(function(){
        var y = $(".content").scrollTop();
        $('.content').animate({scrollTop: y + 150 });
        console.log(y);
     });

    $("#up").click(function(){
        var y = $(".content").scrollTop();
        $('.content').animate({scrollTop: y - 150 });
        console.log(y);
     });

    $(".btn-cerrar").click(function(){
      $('.lateral').animate({"right": -900});
    });

    $(".btn-customize").click(function(){
      if($(".btn-customize").hasClass("btn-29")){
        window.location.href='/customise/say29';
      }else if ($(".btn-customize").hasClass("btn-42")){
        window.location.href='/customise/say42';
      }else{
        window.location.href='/customise/say45';
      }
    });
	}
);