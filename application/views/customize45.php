<div class="capa"></div>

<div class="loader-wrapper">
    <span class="loader">
      	<span class="loader-inner"></span>
  	</span>
</div>

<header class="masthead">
	<div class="container-fluid main my-3">
		<div class="row customize my-1">
			<div id="principal" class="col-12">
				<div id="myCarousel" class="carousel slide carousel-fade w-100" data-slide-to="4" data-interval="false" data-ride="carousel">
		            <div class="carousel-inner" role="listbox">
		                <div class="carousel-item active">
		                    <img src="<?=base_url().'assets/img/say45_back/1.png'?>" class="img-fluid yacht1">
		                </div>
		                <div class="carousel-item">
		                    <img src="<?=base_url().'assets/img/say45_front/1.png'?>" class="img-fluid yacht2">
		                </div>
		                <div class="carousel-item">
		                    <img src="<?=base_url().'assets/img/say45_top/1.png'?>" class="img-fluid yacht3">
		                </div>
		            </div>
		            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					</a>
		        </div>

		        <div class="formulario">
		        	<form action="say45/order" method="POST">
					  <div class="form-row">
					    <div class="form-group col-6">
					      <select id="inputState" class="form-control" name="title" required>
					        <option selected>Mr.</option>
					        <option>Mrs.</option>
					      </select>
					    </div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="firstname">First Name</label>
					    	<input type="text" class="form-control" id="firstname" placeholder="First Name" name="first" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="lastname">Last Name</label>
					    	<input type="text" class="form-control" id="lastname" placeholder="Last Name" name="last" required>
					  	</div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="city">City</label>
					    	<input type="text" class="form-control" id="city" placeholder="City" name="city" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="country">Country</label>
					    	<input type="text" class="form-control" id="country" placeholder="Country" name="country" required>
					  	</div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="email">Email</label>
					    	<input type="text" class="form-control" id="email" placeholder="Email" name="email" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="phone">Phone</label>
					    	<input type="number" class="form-control" id="phone" placeholder="Phone" name="phone">
					  	</div>
					  </div>

					  <div class="form-row">
						  <div class="form-group">
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="termsCheck" name="terms" required>
						      <label class="form-check-label" for="termsCheck">I agree to the <a href="https://saycarbon.com/en/data-protection">terms and conditions</a>. </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="newsCheck" name="news">
						      <label class="form-check-label" for="newsCheck">Yes, I'd like to recieve further marketing communications from SAY Yachts.</label>
						    </div>
						  </div>
					  </div>

					  <button type="submit" class="btn btn-light btn-order">SEND NOW</button>

					  <input type="hidden" id="yachtform" name="yacht"/>
					  <input type="hidden" id="priceform" name="precio"/>
					</form>
		        	
		        </div>
		    </div>
		</div>
	</div>
	<div class="titulo">
		<h1 class="title">CUSTOMISE YOUR SAY</h1>
		<h3 class="subtitle">SAY45 RIB: INCLUDES</h3>
	</div>
	<div class="lateral">
		<button class="btn btn-cerrar">
			<svg xmlns="http://www.w3.org/2000/svg" width="10px" height="40px" viewBox="0 0 50 80" xml:space="preserve">
	    		<polyline fill="none" stroke="#000000" stroke-width="12" stroke-linecap="round" stroke-linejoin="round" points="
				0.375,0.375 45.63,38.087 0.375,75.8 "/>
	  		</svg>
	  	</button>
		<div class="text-lateral">
			<div class="summary">
				<H1>SAY 45 RIB</H1>
				<hr>
				<h4 id="summary">SUMMARY</h4>
				<p class="addons">
						<h3>INCLUDES</h3>
						<p>2x V8 Volvo Penta 6,2 Ltr. 430HP, Z-Drive, Aquamatic Duoprop. <br>
		        		Natural certified Teak decking. <br>
		        		Bimini top with Carbon poles. <br>
		        		Visible Carbon package. <br>
		        		Custom color for soft surfaces. <br>
		        		Custom uni color line. <br>
		        		Custom RAL color on tube. <br>
		        		LED Underwater lighting under swim platform. <br>
		        		Electric swim ladder with hand rail. <br>
		        		Bow Thruster. <br>
						Garmin VHF 300i radio. <br>
		        		LED engine room lightning. <br>
		        		Mooring kit: 2 small + 4 large fenders, 4 lines. <br>
		        		Anti fouling. <br>
						Tailored cockpit persenning. <br> 
						CE Category B Certification incl. necessary modifications. <br> 
					Waterski set.
					</p>
				</div>
			<div class="cost">
				<p class="pricetitle">FINAL AMOUNT</p>
				<p class="price">0 € <i>(Tax Non Inc.)</i></p>
			</div>
			<button class="btn btn-light btn-next">NEXT</button>
		</div>
	</div>

	<div class="more">
		<button class="btn btn-more">
			<svg xmlns="http://www.w3.org/2000/svg" fill="white" width="50" height="50" viewBox="0 0 24 24">
				<path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.094l-4.157-4.104 4.1-4.141-1.849-1.849-4.105 4.159-4.156-4.102-1.833 1.834 4.161 4.12-4.104 4.157 1.834 1.832 4.118-4.159 4.143 4.102 1.848-1.849z"/>
			</svg>
		</button>
	</div>
</header>

<!-- JS -->

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src=<?=$js?>></script>

</body>
</html>