document.querySelectorAll('.menu').forEach(btn => {
    btn.addEventListener('click', e => {
        btn.classList.toggle('active');
    });
});
$(window).on("load",function(){
    $(".loader-wrapper").delay(1000).animate({width: '0px', left: '-10px'}, 800);
});

$(document).ready(function(){
    $(".check").click(function(){
        if( $('.check').is(':checked') ) {
    		$(".navbar").delay(2000).css({"display":"none"});
            $(".textvideo").attr("data-text", "CLOSE");
            $(".capa").delay(3000).css({"display":"none"});
            $("#playboton").css({"display":"none"});
		}else{
			$(".navbar").delay(2000).css({"display":"flex"});
            $(".textvideo").attr("data-text", "WATCH FULL VIDEO");
            $(".capa").delay(2000).css({"display":"block"});
            $("#playboton").css({"display":"block"});
		}
    });

	$(".main").click(function(){
        $("#about").slideUp(500);
        $("#news").slideUp(500);
    	$("#main").delay(500).slideDown(500);
        $(".menu").toggleClass('active');
        $("#navbarResponsive").toggleClass('show');
	});

	$(".about").click(function(){
        $("#main").slideUp(500);
        $("#news").slideUp(500);
    	$("#about").delay(500).slideDown(500);
        $(".menu").toggleClass('active');
        $("#navbarResponsive").toggleClass('show');
	});

    $(".news").click(function(){
        $("#main").slideUp(500);
        $("#about").slideUp(500);
        $("#news").delay(500).slideDown(500);
        $(".menu").toggleClass('active');
        $("#navbarResponsive").toggleClass('show');
    });

    $(".btnclose").click(function(){
        $("#about").slideUp(500);
        $("#news").slideUp(500);
        $("#main").delay(500).slideDown(500);
    });
});