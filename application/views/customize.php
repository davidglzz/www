<div class="capa"></div>

<div class="loader-wrapper">
    <span class="loader">
      	<span class="loader-inner"></span>
  	</span>
</div>

<header class="masthead">
	<div class="container-fluid my-3">
		<div class="row my-2">
			<div class="col-12 title">
				<h3>CUSTOMISE YOUR SAY</h3>
				<h2>SELECT MODEL</h2>
			</div>
		</div>
		<div class="row main my-5">
			<div class="col-12 col-xl-4 say29">
				<div class="card border-0 rounded-0">
					<img class="card-img-top" src="<?=$yacht29?>">
				   	<div class="card-block">
				      	<button class="btn btn-outline-light btn-card">SAY29</button>
				    </div>
				 </div>
			</div>
			<div class="col-12 col-xl-4 say42">
				<div class="card border-0 rounded-0">
					<img class="card-img-top" src="<?=$yacht42?>">
				   	<div class="card-block">
				      	<button class="btn btn-outline-light btn-card">SAY42</button>
				    </div>
				 </div>
			</div>

			<div class="col-12 col-xl-4 say45">
				<div class="card border-0 rounded-0">
					<img class="card-img-top" src="<?=$yacht45?>">
				   	<div class="card-block">
				      	<button class="btn btn-outline-light btn-card">SAY45</button>
				    </div>
				 </div>
			</div>
		</div>
	</div>
	<div class="lateral">
		<button class="btn btn-cerrar">
			<svg xmlns="http://www.w3.org/2000/svg" fill="black" width="5vw" height="5vh" viewBox="0 0 24 24">
				<path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.094l-4.157-4.104 4.1-4.141-1.849-1.849-4.105 4.159-4.156-4.102-1.833 1.834 4.161 4.12-4.104 4.157 1.834 1.832 4.118-4.159 4.143 4.102 1.848-1.849z"/>
			</svg>
	  	</button>
		<div class="text-lateral">
			<button id="up" class="btn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z"/></svg></button>
			<div class="content">
				<h1 class="title-lateral">Hola</h2>
				<p class="subtitle-lateral">SAY Carbon Yachts demonstrating how we can reduce our carbon footprint and remain with a stylish, modern and luxury boating lifestyle without restricting performance<br>

				The combination of the light-weight Carbon hydrodynamic hull and Volvo Penta 430HP low-emission engine excels for breath-taking acceleration to top speeds of 45 knots!<br>

				Every detail on the SAY 29 Carbon is optimized for speed and agility, all components are perfectly matched. The custom wave cutting bow design combined with stability side wings on the bathing platform ensures for maximum steadiness while enabling incredible cornering speeds</p>
			</div>
			<button id="down" class="btn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/></svg></button>
			<button class="btn btn-light btn-customize">START NOW</button>
		</div>
	</div>

	<button onclick="window.history.back();" class="btn-back">
		<svg class="back" xmlns="http://www.w3.org/2000/svg" fill="black" width="40px" height="40px" viewBox="0 0 30 30">
			<path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
			c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
			c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
		</svg>
	</button>
</header>

<!-- JS -->

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src=<?=$js?>></script>

</body>
</html>