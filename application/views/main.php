<!-- Masthead -->
  <div class="loader-wrapper">
    <span class="loader">
      <span class="loader-inner"></span>
    </span>
  </div>

	<div class="capa">
		<video autoplay muted loop id="myVideo">
  			<source src="<?=$video_main?>" type="video/mp4">
		</video>
	</div>
	
 	<header class="masthead">
    	<div class="container h-100">
      		<div class="row h-100 align-items-center justify-content-center text-center">
      			<div id="main" class="col-12">
      				<h1>DESIGN YOUR OWN SAY</h1>
      				<button onclick="window.location.href='/customise'" class="customize">
    					<span class="circle">
				      		<span class="icon arrow">  
                  </span>
				    	</span>
				    	<span class="button-text">CUSTOMISE NOW</span>
				  	</button>
      			</div>
      			<div id="about" class="cardabout col-12" style="display: none;">
      				<div class="card shadow overflow-auto">
                <button class="btnclose">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.094l-4.157-4.104 4.1-4.141-1.849-1.849-4.105 4.159-4.156-4.102-1.833 1.834 4.161 4.12-4.104 4.157 1.834 1.832 4.118-4.159 4.143 4.102 1.848-1.849z"/></svg>
                </button>
      					<div class="card-body">
      						<p>We are SAY Carbon: Your yacht experts for high performance, low emission and electric yachting solutions.<br><br>
                  The entire SAY team possess great knowledge in boatbuilding or the automotive industry – nearly every member of our staff has at least 10 years of on-the-job experience. When you purchase a SAY Carbon yacht, you can rest assured that your model is being designed and constructed by true specialists.<br>
                  <br>
                  We‘re growing constantly!<br><br>
                  You want to be part of our team? Feel free to talk to us!</p>
      					</div>
      				</div>
      			</div>
            <div id="news" class="cardnews col-12" style="display: none;">
              <div class="card shadow overflow-auto">
                <button class="btnclose">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.094l-4.157-4.104 4.1-4.141-1.849-1.849-4.105 4.159-4.156-4.102-1.833 1.834 4.161 4.12-4.104 4.157 1.834 1.832 4.118-4.159 4.143 4.102 1.848-1.849z"/></svg>
                </button>
                <div class="card-body">
                  <p>Subscribe to our newsletter</p>
                  <form action="main/news" method="POST">
                    <div class="form-row">
                      <div class="form-group col-12">
                        <input type="text" class="form-control" id="email" placeholder="Email" name="email" required>
                        <button type="submit" class="btn btn-light">SUBSCRIBE</button>
                        <a href="https://saycarbon.com/en/data-protection/">Data Protection</a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
        	</div>
    	</div>
      <div class="row">
            <div class="col-1 col-sm-6 col-md-7 col-lg-8 col-xl-9"></div>
            <div class="colvideo col-11 col-sm-6 col-md-5 col-lg-4 col-xl-3">
              <div class="wrapper">
                <input id="myCheck" class="check" type="checkbox">
                  <!--<div class="video">
                   <video class="videomp4" muted loop paused controls>
                     <source src="<?=$video_full?>" type="video/mp4">
                   </video>
                  </div>-->
                  <div class="text">
                         <span class="textvideo" data-text="WATCH FULL VIDEO">
                          <img id="playboton" src="<?=base_url().'assets/img/play_boton.png'?>" width="45px" height="45px" padding-right="50px">
                        </span>
                  </div>
              </div>
            </div>
          </div>
    	

  	</header>
	
<!-- End Info -->

<!-- JS -->

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src=<?=$js?>></script>

</body>
</html>