<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *'); 
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta name="mobile-web-app-capable" content="yes">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?=$css?>">
	<title><?=$title?></title>
</head>
<body id="page-top">
<!-- Start Navigation -->
	
	<nav class="navbar navbar-toggler-lg navbar-light fixed-top noselect" id="mainNav">
		<a id="home" class="navbar-brand" href="/">
			<img src="<?=$logo_say?>">
		</a>
			
		<button id="botonMenu" class="menu navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<svg viewBox="0 0 64 48">
				<path d="M19,15 L45,15 C70,15 58,-2 49.0177126,7 L19,37"></path>
				<path d="M19,24 L45,24 C61.2371586,24 57,49 41,33 L32,24"></path>
				<path d="M45,33 L19,33 C-8,33 6,-2 22,14 L45,37"></path>
			</svg>
		</button>

		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link main">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link about">About Us</a>
				</li>
				<li class="nav-item">
					<a class="nav-link news">Newsletter</a>
				</li>
			</ul>
		</div>
	</nav>

<!-- End Navigation -->