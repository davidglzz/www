<div class="capa"></div>

<div class="loader-wrapper">
    <span class="loader">
      	<span class="loader-inner"></span>
  	</span>
</div>

<header class="masthead">
	<div class="container-fluid main my-3">
		<div class="row customize my-1">
			<div id="principal" class="col-12">
				<div id="myCarousel" class="carousel slide carousel-fade w-100" data-slide-to="4" data-interval="false" data-ride="carousel">
		            <div class="carousel-inner" role="listbox">
		                <div class="carousel-item active">
		                    <img src="<?=base_url().'assets/img/say42_back/1/1/5.png'?>" class="img-fluid yacht1">
		                </div>
		                <div class="carousel-item">
		                    <img src="<?=base_url().'assets/img/say42_front/1/1/5.png'?>" class="img-fluid yacht2">
		                </div>
		                <div class="carousel-item">
		                    <img src="<?=base_url().'assets/img/say42_top/1/1/5.png'?>" class="img-fluid yacht3">
		                </div>
		            </div>
		            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					</a>
		        </div>

		        <div class="option-addons w-100">
		        	<button id="up" class="btn btn-up"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z"/></svg></button>
		        	<div id="add" class="option-addon">
		        		<div class="option-text">
		        			<h3>Basic Hardware</h3>
		        			<p>	
	
		        				Cupholders <br>
		        				Retractable cleats, 8pcs <br>
		        				Suspension seats with leg and height adjusters, 2 pcs <br>
		        				Manual swim ladder <br>
		        				Owners cabin <br>
		        				Fully equipped bathrom <br>
		        				Wetbar <br>
		        				Electric tale pedestal <br>
		        				Shower on deck <br>
		        				Electric engine hatch <br> <br>

		        			</p>
		        			<h3>Comfort Systems</h3>
		        			<p>
		        				12V and USB charging outlets <br>
		        				Engine battery + seperate service battery  <br>
		        				Automatic bilge pumps, 3 pcs <br>
		        				LED lighting on deck and interior <br>
								Fusion entertainment system incl. 4 speakers on deck, 2 inside BT and USB <br> 
								Fresh water shower on deck, 110ltr. capacity <br> 
								Garmin 8416 plotter incl. echo-sounder & CZone CAN Bus control system <br>
								Refrigerator, 60ltr. <br>
								Shore power connection incl. battery charger <br><br>
							</p>
							<h3>Accessories</h3>
							<p>
		        				Prescribed safety equipment and lighting <br>
		        				Windlass with concealed anchor and chain <br>
		        				Fire extinguisher  <br>
		        			</p>
		        		</div>
		        		<div id="1" class="option-overlay"></div>
		        	</div>
		        	<div id="hard" class="option-addon">
		        		<div class="option-text">
		        			<h3>Hardware</h3>
		        			<button id="hard1" class="btn btn-light btn-addon">Natural certified Teak decking</button>
		        			<button id="hard2" class="btn btn-light btn-addon">Bimini top with carbon poles</button>      		
		        		</div>
		        		<div id="2" class="option-overlay"></div>
		        	</div>
		        	<div id="comfort" class="option-addon">
		        		<div class="option-text">
		        			<h3>Comfort Systems</h3>
		        			<button id="comfort1" class="btn btn-light btn-addon">Wine cooler in cabin</button>
		        			<button id="comfort2" class="btn btn-light btn-addon">Upgrade audio system with amplifier, 6 high class speakers + subwoofer</button>
		        			<button id="comfort3" class="btn btn-light btn-addon">Garmin 8416, additional screen</button>
		        			<button id="comfort4" class="btn btn-light btn-addon">LED underwater lighting under swim platform</button>
		        			<button id="comfort5" class="btn btn-light btn-addon">Electric swim ladder with hand rail</button>
		        			<button id="comfort6" class="btn btn-light btn-addon">DAB+ antenna</button>	
		        			<button id="comfort7" class="btn btn-light btn-addon">Lightweight AGM batteries (-30kg</button>	
		        			<button id="comfort8" class="btn btn-light btn-addon">Bow thruster</button>	
		        			<button id="comfort9" class="btn btn-light btn-addon">Garmin VHF 300i radio</button>	
		        			<button id="comfort10" class="btn btn-light btn-addon">FLIR M132 Night vision (requires T-top / Mast)</button>	
		        			<button id="comfort11" class="btn btn-light btn-addon">Radar Garmin Fantom 24 (requires T-top / Mast)</button>	
		        			<button id="comfort12" class="btn btn-light btn-addon">SeaKeeper 2 HD stabilizer incl. Li-batteries and structural reinforcement</button>
		        			<button id="comfort13" class="btn btn-light btn-addon">Quick MC² X3 stabilizer incl. Li-batteries and structural reinforcement</button>	
		        			<button id="comfort14" class="btn btn-light btn-addon">Rain shower on deck</button>	
		        			<button id="comfort15" class="btn btn-light btn-addon">Hot water system for bathroom</button>	 
		        			<button id="comfort16" class="btn btn-light btn-addon">Lenco trim-tabs</button> 	        		
		        		</div>
		        		<div id="3" class="option-overlay"></div>
		        	</div>
		        	<div id="acce" class="option-addon">
		        		<div class="option-text">
		        			<h3>Accessories</h3>
		        			<button id="acce1" class="btn btn-light btn-addon">Mooring kit: 2 small + 2 large fender, 4 lines</button>
		        			<button id="acce2" class="btn btn-light btn-addon">Antifouling</button>
		        			<button id="acce3" class="btn btn-light btn-addon">CE Category B certification incl. neccessary modification</button>
		        			<button id="acce4" class="btn btn-light btn-addon">Water ski set incl. towing eye, mirror, line and sport vest</button>
		        			<button id="acce5" class="btn btn-light btn-addon">Tailored cockpit persenning</button>
		        			<button id="acce6" class="btn btn-light btn-addon">SAY tool set</button>			        		
		        		</div>
		        		<div id="4" class="option-overlay"></div>
		        	</div>
		        	<div id="roof" class="option-addon">
		        		<div class="option-text">
		        			<h3>Roof</h3>
		        			<button id="roof1" class="btn btn-light btn-addon">T-top</button>
		        			<button id="roof2" class="btn btn-light btn-addon">T-top + aft bimini</button>
		        			<button id="roof3" class="btn btn-light btn-addon">Mast</button>
		        			<button id="roof4" class="btn btn-light btn-addon">Mast + AFT and cockpit bimini</button>		        		
		        		</div>
		        		<div id="5" class="option-overlay"></div>
		        	</div>
		        	<div class="option-addon-control">
		        		<button class="option-addon-controls opt-1">ADD MORE ADD-ONS</button>
		        		<button class="option-addon-controls opt-2">HARDWARE</button>
		        		<button class="option-addon-controls opt-3">COMFORT</button>
		        		<button class="option-addon-controls opt-4">ACCESSORIES</button>
		        		<button class="option-addon-controls opt-5">ROOF</button>
		        	</div>
		        	<button id="down" class="btn btn-down"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/></svg></button>
		        </div>

		        <div class="formulario">
		        	<form action="say42/order" method="POST">
					  <div class="form-row">
					    <div class="form-group col-6">
					      <select id="inputState" class="form-control" name="title" required>
					        <option selected>Mr.</option>
					        <option>Mrs.</option>
					      </select>
					    </div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="firstname">First Name</label>
					    	<input type="text" class="form-control" id="firstname" placeholder="First Name" name="first" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="lastname">Last Name</label>
					    	<input type="text" class="form-control" id="lastname" placeholder="Last Name" name="last" required>
					  	</div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="city">City</label>
					    	<input type="text" class="form-control" id="city" placeholder="City" name="city" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="country">Country</label>
					    	<input type="text" class="form-control" id="country" placeholder="Country" name="country" required>
					  	</div>
					  </div>

					  <div class="form-row">
					  	<div class="form-group col-4">
					  		<label for="email">Email</label>
					    	<input type="text" class="form-control" id="email" placeholder="Email" name="email" required>
					  	</div>
					  	<div class="form-group col-4">
					  		<label for="phone">Phone</label>
					    	<input type="number" class="form-control" id="phone" placeholder="Phone" name="phone" required>
					  	</div>
					  </div>

					  <div class="form-row">
						  <div class="form-group">
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="termsCheck" name="terms" required>
						      <label class="form-check-label" for="termsCheck">I agree to the terms and conditions.</label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="newsCheck" name="news">
						      <label class="form-check-label" for="newsCheck">Yes, I'd like to recieve further marketing communications from SAY Yachts.</label>
						    </div>
						  </div>
					  </div>

					  <button type="submit" class="btn btn-light btn-order">SEND NOW</button>

					  <input type="hidden" id="yachtform" name="yacht"/>
					  <input type="hidden" id="engineform" name="engine"/>
					  <input type="hidden" id="colourform" name="colourscheme"/>
					  <input type="hidden" id="upholsteryform" name="upholstery"/>
					  <input type="hidden" id="floorform" name="floor"/>
					  <input type="hidden" id="addonsform" name="addon"/>
					  <input type="hidden" id="numcolor" name="numcolor"/>
					  <input type="hidden" id="numtapi" name="numtapi"/>
					  <input type="hidden" id="priceform" name="precio"/>
					</form>
		        	
		        </div>
		    </div>
			<div class="options col-12">

				<div class="option-fuel">
					<button id="option-fuel1" class=" btn-engine" style="background-image: url('/assets/img/engine42/motor_b_1.png');"></button>
			    	<button id="option-fuel2" class=" btn-engine" style="background-image: url('/assets/img/engine42/motor_b_2.png');"></button>
			    	<button id="option-fuel3" class=" btn-engine" style="background-image: url('/assets/img/engine42/motor_b_3.png');"></button>
			    	<button id="option-fuel4" class=" btn-engine" style="background-image: url('/assets/img/engine42/motor_b_4.png');"></button>
				</div>

				<div class="option-group-color">
					<button id="option-group-color1" class="btn-group-color">METALLIC COLLECTION</button>
			    	<button id="option-group-color2" class="btn-group-color">SAY LIFESTYLE</button>
			    	<button id="option-group-color3" class="btn-group-color">PURE COLLECTION</button>
			    	<button id="option-group-color4" class="btn-group-color">RACING COLLECTION</button>
				</div>

				<div class="option-color">
					<button id="option-color1" class="btn-color" style="background-image: url('/assets/img/boton/botton001.png');"></button>
			    	<button id="option-color2" class="btn-color" style="background-image: url('/assets/img/boton/botton002.png');"></button>
			    	<button id="option-color3" class="btn-color" style="background-image: url('/assets/img/boton/botton003.png');"></button>
			    	<button id="option-color4" class="btn-color" style="background-image: url('/assets/img/boton/botton004.png');"></button>
			    	<button id="option-color5" class="btn-color" style="background-image: url('/assets/img/boton/botton005.png'); display: none;"></button>
			    	<button id="option-color6" class="btn-color" style="background-image: url('/assets/img/boton/botton006.png'); display: none;"></button>
			    	<button id="option-color7" class="btn-color" style="background-image: url('/assets/img/boton/botton007.png'); display: none;"></button>
			    	<button id="option-color8" class="btn-color" style="background-image: url('/assets/img/boton/botton008.png'); display: none;"></button>
			    	<button id="option-color9" class="btn-color" style="background-image: url('/assets/img/boton/botton009.png'); display: none;"></button>
			    	<button id="option-color10" class="btn-color" style="background-image: url('/assets/img/boton/botton010.png'); display: none;"></button>
			    	<button id="option-color11" class="btn-color" style="background-image: url('/assets/img/boton/botton011.png'); display: none;"></button>
			    	<button id="option-color12" class="btn-color" style="background-image: url('/assets/img/boton/botton012.png'); display: none;"></button>
			    	<button id="option-color13" class="btn-color" style="background-image: url('/assets/img/boton/botton013.png'); display: none;"></button>
				</div>

				<div class="option-tapiceria">
					<button id="option-tapiceria1" class="btn-huls img-fluid" style="background-image: url('/assets/img/huls/01.png');"></button>
			    	<button id="option-tapiceria2" class="btn-huls img-fluid" style="background-image: url('/assets/img/huls/02.png');"></button>
			    	<button id="option-tapiceria3" class="btn-huls img-fluid" style="background-image: url('/assets/img/huls/03.png');"></button>
			    	<button id="option-tapiceria4" class="btn-huls img-fluid" style="background-image: url('/assets/img/huls/04.png');"></button>
			    	<button id="option-tapiceria5" class="btn-huls img-fluid" style="background-image: url('/assets/img/huls/05.png');"></button>
			    	<button id="option-tapiceria6" class="btn-huls" style="background-image: url('/assets/img/huls/06.png');"></button>
			    	<button id="option-tapiceria7" class="btn-huls" style="background-image: url('/assets/img/huls/07.png');"></button>
			    	<button id="option-tapiceria8" class="btn-huls" style="background-image: url('/assets/img/huls/08.png');"></button>
				</div>
			    
		    </div>
		</div>
	</div>
	<div class="titulo">
		<h1 class="title">CUSTOMISE YOUR SAY</h1>
		<h3 class="subtitle">SAY42: ENGINE</h3>
	</div>
	<div class="lateral">
		<button class="btn btn-cerrar">
			<svg xmlns="http://www.w3.org/2000/svg" width="10px" height="40px" viewBox="0 0 50 80" xml:space="preserve">
	    		<polyline fill="none" stroke="#000000" stroke-width="12" stroke-linecap="round" stroke-linejoin="round" points="
				0.375,0.375 45.63,38.087 0.375,75.8 "/>
	  		</svg>
	  	</button>
		<div class="text-lateral">
			<button class="btn btn-light btn-back disabled">GO BACK</button>
			<div class="content">
				<h2 id="title">SAY 42 Carbon</h2>
				<p id="subtitle">The new SAY 42R Carbon is another breakthrough of low-emission cruising with high-performance handling. Its lightweight hull design enabling speeds above 50 knots (Volvo Penta).</p>
				<div class="option-teca col-12">
					<button id="option-teca1" class="btn-teca" style="background-image: url('/assets/img/teca/teak_button.png')";></button>
					<button id="option-teca2" class="btn-teca" style="background-image: url('/assets/img/teca/seatek_button.png')";></button>
				</div>
			</div>
			<div class="summary">
				<hr>
				<h4 id="summary">SUMMARY</h4>
				<p class="fuel"></p>
				<p class="color"></p>
				<p class="tapi"></p>
				<p class="teca"></p>
				<p class="addons"></p>
			</div>
			<div class="cost">
				<p class="pricetitle">FINAL AMOUNT</p>
				<p class="price">0 € <i>(Tax Non Inc.)</i></p>
			</div>
			<button class="btn btn-light btn-next">NEXT</button>
		</div>
	</div>

	<div class="more">
		<button class="btn btn-more">
			<svg xmlns="http://www.w3.org/2000/svg" fill="white" width="50" height="50" viewBox="0 0 24 24">
				<path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.094l-4.157-4.104 4.1-4.141-1.849-1.849-4.105 4.159-4.156-4.102-1.833 1.834 4.161 4.12-4.104 4.157 1.834 1.832 4.118-4.159 4.143 4.102 1.848-1.849z"/>
			</svg>
		</button>
	</div>
</header>

<!-- JS -->

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src=<?=$js?>></script>

</body>
</html>