document.querySelectorAll('.menu').forEach(btn => {
    btn.addEventListener('click', e => {
        btn.classList.toggle('active');
    });
});

var step=0, precioinicial=557670.51, addons = [];

$(window).on("load",function(){
    $(".loader-wrapper").delay(2000).animate({width: '0px', left: '-20px'}, 800);
});

function breathSubtitle(text){
  $(".subtitle").animate({fontSize: '+=5rem!important'}, 500);
  $(".subtitle").text(text);
  $(".subtitle").animate({fontSize: '-=5rem!important'}, 500);
}

function breath(){
  $(".content").fadeOut();
  $(".content").fadeIn();
  $(".summary").fadeOut();
  $(".summary").fadeIn();
}

function extrasSelected(){
  $('.addons').text("");
  $(".btn-success").each(function(){
    var id = $(this).attr('id');
    var text = $("#"+id).text();
    addons.push(text);
    $('.addons').append("Extra: "+text+"<br><br>");
  });

  $('.summary').animate({height: "100vh"});
}

function addValues(numero){
  $('#yachtform').val('SAY 45 RIB');
  $('#priceform').val(numero);
}

$(document).ready(

	function(){
    $(".price").text(precioinicial.toLocaleString()+' €').append('<i> (Tax Non Inc.)</i>');
    $('.btn-next').attr("disabled", false);
    $('#botonMenu').slideUp();
    $('.masthead').animate({"opacity": 1},1000);
    //$('.carousel').slideUp("fast");
    $('.option-group-color').slideUp("fast");
    $('.option-color').slideUp("fast");
    $('.option-tapiceria').slideUp("fast");
    $('.option-teca').slideUp("fast");
    $('.formulario').slideUp("fast");
    $('.option-addons').slideUp("fast");
    $('#add').slideUp("fast");
    $('#comfort').slideUp("fast");
    $('#hard').slideUp("fast");
    $('#acce').slideUp("fast");
    $('#summary').slideUp("fast");


    $(".btn-cerrar").click(function(){
      $('.lateral').animate({"right": -900});
      $('.main').animate({left: '0vw'});
      $('.main').animate({width: '100vw'});
      $('.customize').animate({left: '7vw'});
      $('.customize').animate({width: '100vw'});
      $('.formulario').slideUp();
      $('.titulo').addClass('invisible');
    });

    $(".btn-more").click(function(){
      $('.lateral').animate({"right": 0});
      $('.main').animate({left: '-4vw'});
      $('.main').animate({width: '70vw'});
      $('.customize').animate({left: '-4vw'});
      $('.customize').animate({width: '70vw'});
      $('.formulario').slideDown();
      $('.titulo').removeClass('invisible');
    });


    //Menú derecho
    $('.btn-next').click(function(){
      $('.formulario').slideDown();
      $('.btn-next').slideUp();
    });

     $('.btn-order').click(function(){
      preciofinal = precioinicial;
      addValues(preciofinal);
     });
  }   
);